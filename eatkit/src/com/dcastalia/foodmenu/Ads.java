package com.dcastalia.foodmenu;

public class Ads {

    String id;
    String title;
    String desc;
    String link;
    String order;
    String image;
    
    public Ads(String id, String title, String desc, String link, String order, String image){
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.link = link;
        this.order = order;
        this.image = image;
    }
}
