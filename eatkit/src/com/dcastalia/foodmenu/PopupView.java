package com.dcastalia.foodmenu;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.image.SmartImageView;

public class PopupView extends Activity implements OnClickListener{

	private Button close;
	private Button add2bkct;
	private TextView ItemName;
	private TextView ItemDesc;
	private TextView FoodPrice;
	private SmartImageView ItemImage;
	private MenuParser Menuparser;
	public  Global global;
	private int menuid;
	private int itemid;
	private String[] checkvalue;
	private Button spinnerButton;
	private ListView SubList;
	float rx,ry;
	List<Model> list;
	DataHelper dh;
	int spinnerPOS;
	OptionsParser OP;
	public void setElements(){
		
		LinearLayout popup_laLayout = (LinearLayout)findViewById(R.id.popup_container);
		popup_laLayout.setLayoutParams(new LinearLayout.LayoutParams((int)(300*rx),(int)(415*ry)));
		popup_laLayout.setPadding((int)(10*rx),0,0,(int)(0*ry));
		close = (Button)findViewById(R.id.close);
		close.setOnClickListener(close_button_listener);
		LayoutParams close_params = new LinearLayout.LayoutParams((int)(32*rx),(int)(32*ry));
		close.setLayoutParams(close_params);
		ItemImage = (SmartImageView)findViewById(R.id.ItemImage);
		LayoutParams item_image_params = new LinearLayout.LayoutParams((int)(220*rx),(int)(160*ry));
		item_image_params.setMargins(0, (int)(10*ry), 0, 0);
		ItemImage.setLayoutParams(item_image_params);
		ItemName = (TextView)findViewById(R.id.ItemName);
		ItemName.setTextSize((20f*ry)/global.f);
		ScrollView itemdesc_scroller = (ScrollView)findViewById(R.id.item_desc_scroll);
		itemdesc_scroller.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT,(int)(32*ry)));
		LinearLayout checkbox_laLayout = (LinearLayout)findViewById(R.id.checkboxLayout);
		LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,(int)(85*ry));
		checkbox_laLayout.setLayoutParams(lp);
		//lp.setMargins(0, (int)(90*ry), 0, 0);
		
		TextView qtyText = (TextView)findViewById(R.id.qty_textView);
		qtyText.setTextSize((12f*ry)/global.f);
		spinnerButton = (Button)findViewById(R.id.drop_down_button);
		spinnerButton.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				global.setSpinnerposition(0);
				startActivity(new Intent("SpinnerDialog"));
			}

		});
		spinnerButton.setLayoutParams(new LinearLayout.LayoutParams((int)(80*rx),(int)(42*ry)));
		add2bkct = (Button)findViewById(R.id.add2bckt_button);
		add2bkct.setOnClickListener(add2bkct_button_listener);
		add2bkct.setLayoutParams(new LinearLayout.LayoutParams((int)(80*rx),(int)(42*ry)));
		add2bkct.setTextSize((12*ry)/global.f);
		ItemDesc = (TextView)findViewById(R.id.ItemDesc);
		ItemDesc.setTextSize((14f*ry)/global.f);
		FoodPrice = (TextView)findViewById(R.id.Foof_Price);
		FoodPrice.setTextSize((16f*ry)/global.f);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.popup_layout);
		global = new Global();
		rx = global.getRx();
		ry = global.getRy();
		setElements();
		OP = global.getOptionsparser();
		global.setDatahelper(new DataHelper(this));
		dh = global.getDatahelper();
		spinnerPOS = 0;
		global.setSpinnerposition(spinnerPOS);
		//        amount = new String[20];



		//        for(int i=0;i<amount.length;i++){
		//            amount[i] = "" + (i+1);
		//        }
		total_item_price = 0;

		//spin.getWindowVisibleDisplayFrame();
		//spin.requestRectangleOnScreen((new Rect(0,0,100,100)),true);
		Menuparser = global.getMenuparser();
		menuid = global.getMenuId();
		itemid = global.getItemId();
		if(global.getFeaturedItemClicked())
			initiateFeaturePopup();
		else
			initiatePopupWindow();
	} 

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if(hasFocus){
			spinnerPOS = global.getSpinnerposition();
			spinnerButton.setText((spinnerPOS+1)+"");
		}
	}

	private void initiatePopupWindow() {
		try {

			ItemImage.setImageUrl(Menuparser.mlist.get(menuid).item.get(itemid).getImage());
			ItemName.setText(Menuparser.mlist.get(menuid).item.get(itemid).getName());
			ItemDesc.setText(Menuparser.mlist.get(menuid).item.get(itemid).getDesc());
			FoodPrice.setText("Price: "+ Menuparser.mlist.get(menuid).item.get(itemid).getPrice());
			//checklayout = (LinearLayout)layout.findViewById(R.id.checkboxLayout);

			//Array not emepty
			if(Menuparser.mlist.get(menuid).item.get(itemid).subitem.size()>0){
				checkvalue = new String[Menuparser.mlist.get(menuid).item.get(itemid).subitem.size()];
				for(int i=0;i<Menuparser.mlist.get(menuid).item.get(itemid).subitem.size();i++){

					checkvalue[i] = Menuparser.mlist.get(menuid).item.get(itemid).subitem.get(i).getName()
							+"  "+Menuparser.mlist.get(menuid).item.get(itemid).subitem.get(i).getPrice()
							+"  "+Menuparser.mlist.get(menuid).item.get(itemid).subitem.get(i).getUnit();

				}
			}

			SubList = (ListView)findViewById(R.id.sublist);
			ArrayAdapter<Model> adapter = new InteractiveArrayAdapter(this,
					getModel());
			SubList.setAdapter(adapter);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initiateFeaturePopup() {
		try {

			ItemImage.setImageUrl(OP.flist.get(global.getFeatureId()).image);
			ItemName.setText(OP.flist.get(global.getFeatureId()).name);
			ItemDesc.setText(OP.flist.get(global.getFeatureId()).desc);
			FoodPrice.setText("Price: "+ OP.flist.get(global.getFeatureId()).price);
			//checklayout = (LinearLayout)layout.findViewById(R.id.checkboxLayout);

			//Array not emepty
			if(OP.flist.get(global.getFeatureId()).subitem.size()>0){
				checkvalue = new String[OP.flist.get(global.getFeatureId()).subitem.size()];
				for(int i=0;i<OP.flist.get(global.getFeatureId()).subitem.size();i++){

					checkvalue[i] = OP.flist.get(global.getFeatureId()).subitem.get(i).getName()
							+"  "+OP.flist.get(global.getFeatureId()).subitem.get(i).getPrice()
							+"  "+OP.flist.get(global.getFeatureId()).subitem.get(i).getUnit();

				}
			}

			SubList = (ListView)findViewById(R.id.sublist);
			ArrayAdapter<Model> adapter = new InteractiveArrayAdapter(this,
					getModel());
			SubList.setAdapter(adapter);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<Model> getModel() {
		list = new ArrayList<Model>();
		for(int i=0;i<checkvalue.length;i++){
			list.add(get(checkvalue[i]));
		}
		return list;
	}

	private Model get(String s) {
		return new Model(s);
	}

	public void onClick(View v) {

	}

	String description;
	double total_item_price;
	String food_name;
	int id_food;
	int id_cat;
	int amount;
	double food_price;

	public void regularItem(){
		description = "";
		total_item_price = 0;
		food_name = Menuparser.mlist.get(menuid).item.get(itemid).getName();
		id_food = Integer.parseInt(Menuparser.mlist.get(menuid).item.get(itemid).getId());
		id_cat = Integer.parseInt(Menuparser.mlist.get(menuid).getId());
		amount = spinnerPOS+1;
		food_price = Double.parseDouble(Menuparser.mlist.get(menuid).item.get(itemid).getPrice());
		try{
			//checkouttext.setText(""+click_count+click_text+"\n"+s+"\n");
			boolean selected = false;
			for(int i=0;i<list.size();i++){
				if(list.get(i).isSelected()){

					/**
					 * String food_name = Menuparser.mlist.get(menuid).item.get(itemid).getName()
					 * int id_cat = Menuparser.mlist.get(menuid).getId()
					 * int amount = sprinnerPOS
					 * String description = String s
					 * double item_price = sum of selected item price //Menuparser.mlist.get(menuid).item.get(itemid).subitem.get(i).getPrice() 
					 * double food_price = Menuparser.mlist.get(menuid).item.get(itemid).getPrice()
					 * double total_price) = amount*(item_price + food_price)
					 **/

					selected = true;

					description+= Menuparser.mlist.get(menuid).item.get(itemid).subitem.get(i).getName()+" ";
					total_item_price+=Double.parseDouble(Menuparser.mlist.get(menuid).item.get(itemid).subitem.get(i).getPrice());

				}
			}
			if(!selected){
				description = "";
				total_item_price = 0;
			}

		}catch(Exception e){

		}
		double total_price = amount*(total_item_price+food_price);

		List<String>table = dh.selectAll();
		String temp[];
		boolean updated = false;
		for(String x : table){
			temp = x.split("-_-");
			if(
					temp[2].equals(food_name) &&
					temp[5].equals(description)
					){
				int new_amount = Integer.parseInt(temp[4])+amount;
				double new_total = Double.parseDouble(temp[7])+total_price;
				dh.updateRow(Long.parseLong(temp[0]),
						temp[1],
						temp[2],
						temp[3],
						new_amount+"",
						temp[5],
						""+total_item_price,
						temp[6], 
						new_total+"");
				updated = true;

			}
		}
		if(!updated){
			dh.insert(""+id_food, ""+food_name, ""+id_cat, ""+amount, 
					""+description, ""+total_item_price, ""+food_price, ""+total_price);
		//		global.setBckt("|"+food_name+" "+food_price+"|\n");
		global.setCount(global.getCount()+1);
		global.addItem_amount(amount);
		global.addItem_name(food_name);
		global.addItem_price(total_price);
		}
		finish();  
	}
	public void featuredItem(){
		description = "";
		total_item_price = 0;
		food_name = OP.flist.get(global.getFeatureId()).getName();
		id_food = Integer.parseInt(OP.flist.get(global.getFeatureId()).getId());
		id_cat = -1;//featured items has no specific category id
		amount = spinnerPOS+1;
		food_price = Double.parseDouble(OP.flist.get(global.getFeatureId()).getPrice());
		try{
			//checkouttext.setText(""+click_count+click_text+"\n"+s+"\n");
			boolean selected = false;
			for(int i=0;i<list.size();i++){
				if(list.get(i).isSelected()){

					/**
					 * String food_name = Menuparser.mlist.get(menuid).item.get(itemid).getName()
					 * int id_cat = Menuparser.mlist.get(menuid).getId()
					 * int amount = sprinnerPOS
					 * String description = String s
					 * double item_price = sum of selected item price //Menuparser.mlist.get(menuid).item.get(itemid).subitem.get(i).getPrice() 
					 * double food_price = Menuparser.mlist.get(menuid).item.get(itemid).getPrice()
					 * double total_price) = amount*(item_price + food_price)
					 **/

					selected = true;

					description+= OP.flist.get(global.getFeatureId()).subitem.get(i).getName()+" ";
					total_item_price+=Double.parseDouble(OP.flist.get(global.getFeatureId()).subitem.get(i).getPrice());
				}
			}
			if(!selected){
				description = "";
				total_item_price = 0;
			}

		}catch(Exception e){
		}
		double total_price = amount*(total_item_price+food_price);
		List<String>table = dh.selectAll();
		String temp[];
		boolean updated = false;
		for(String x : table){
			temp = x.split("-_-");
			if(
					temp[2].equals(food_name) &&
					temp[5].equals(description)
					){
				int new_amount = Integer.parseInt(temp[4])+amount;
				double new_total = Double.parseDouble(temp[7])+total_price;
				dh.updateRow(Long.parseLong(temp[0]),
						temp[1],
						temp[2],
						temp[3],
						new_amount+"",
						temp[5],
						""+total_item_price,
						temp[6], 
						new_total+"");
				updated = true;

			}
		}
		if(!updated){
			dh.insert(""+id_food, ""+food_name, ""+id_cat, ""+amount, 
					""+description, ""+total_item_price, ""+food_price, ""+total_price);
		//global.setBckt("|"+food_name+" "+food_price+"|\n");
		//global.setCount(global.getCount()+1);
		global.addItem_amount(amount);
		global.addItem_name(food_name);
		global.addItem_price(total_price);
		global.setCount(global.getCount()+1);
		}
		finish();  
	}
	//	ArrayList<Tobacco> TobaccoList = global.getTobaccoList();
	public void tobaccoItem(){
		food_name = Menuparser.mlist.get(menuid).item.get(itemid).getName();
		id_food = Integer.parseInt(Menuparser.mlist.get(menuid).item.get(itemid).getId());
		id_cat = Integer.parseInt(Menuparser.mlist.get(menuid).getId());
		amount = spinnerPOS+1;
		food_price = Double.parseDouble(Menuparser.mlist.get(menuid).item.get(itemid).getPrice());
		try{
			//checkouttext.setText(""+click_count+click_text+"\n"+s+"\n");
			boolean selected = false;
			for(int i=0;i<list.size();i++){
				if(list.get(i).isSelected()){
					selected = true;
					description+= Menuparser.mlist.get(menuid).item.get(itemid).subitem.get(i).getName()+" ";
					total_item_price+=Double.parseDouble(Menuparser.mlist.get(menuid).item.get(itemid).subitem.get(i).getPrice());
				}
			}
			if(!selected){
				description = "";
				total_item_price = 0;
			}
		}catch(Exception e){		}
		double total_price = amount*(total_item_price+food_price);

		boolean updated = false;
		for(int i=0;i<global.getTobaccoList().size();i++){
			
			if(
					global.getTobaccoList().get(i).id_food == id_food &&
							(global.getTobaccoList().get(i).food_name).equals(food_name) &&
					(global.getTobaccoList().get(i).desc).equals(description)
					){
				int new_amount = global.getTobaccoList().get(i).amount+amount;
				double new_total = global.getTobaccoList().get(i).TotalPrice+total_price;
				global.getTobaccoList().get(i).amount = new_amount;
				global.getTobaccoList().get(i).TotalPrice = new_total;
				updated = true;

			}
		}
		if(!updated){
		(global.getTobaccoList()).add(new Tobacco(
				global.getTobaccoList().size()-1
				,id_food,food_name,id_cat,amount,description,total_item_price,food_price,total_price));
		//		global.setTobaccoList(TobaccoList);
		global.addItem_amount(amount);
		}
		finish();
	}
	private OnClickListener add2bkct_button_listener = new OnClickListener() {
		public void onClick(View v) {
			if(global.getFeaturedItemClicked()){
				featuredItem();
			}/*else if((Menuparser.mlist.get(global.getMenuId()).name).equals("BENSON & HEDGES")){
				if(global.isOnline){tobaccoItem();} else{Toast toast = Toast.makeText(PopupView.this,"There is no Connection. Please try it online.", 5000); toast.show();}
			}*/
			else{
				regularItem();
			}
		}
	};
	private OnClickListener close_button_listener = new OnClickListener() {
		public void onClick(View v) {
			finish();
		}
	};
	@Override  
	public void onBackPressed() {
		//Do Nothing
	}
	@Override
	public void finish() {
		super.finish();
		if(dh!=null)
			dh.close();
	}
}
