package com.dcastalia.foodmenu;

public class Commerce {

    String vat;
    String service;
    
    public Commerce(String vat, String service){
        this.vat = vat;
        this.service = service;
    }
    
    public String getVat(){
        return vat;
    }
    
    public String getService(){
        return service;
    }
}
