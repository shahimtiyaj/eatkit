package com.dcastalia.foodmenu;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class OccupationSpinnerDialog extends Activity{
	Global global;
	float rx,ry;
	Button close_button;
	String values[]={
			"Student",
			"Service in MNC",
			"Work in Agency",
			"Service in Local Corporate",
			"Architect",
			"Engineer",
			"Doctor",
			"Other"
			};
	public void setElements(){
		LinearLayout main_spin_layout = (LinearLayout)findViewById(R.id.main_spin_layout);
		main_spin_layout.setLayoutParams(new LayoutParams((int)(280*rx),(int)(350*ry)));
		LinearLayout top_spin_layout = (LinearLayout)findViewById(R.id.top_layout);
		top_spin_layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,(int)(60*ry)));
		ImageView ic_menu_imageview = (ImageView)findViewById(R.id.ic_menu_imageview);
		ic_menu_imageview.setLayoutParams(new LayoutParams((int)(50*rx),(int)(50*ry)));
		TextView occu_select_textView = (TextView)findViewById(R.id.occu_textview);
		LayoutParams occu_select_params = new LayoutParams((int)(180*rx),LayoutParams.FILL_PARENT);
//		amount_select_params.setMargins((int)(50*rx),0,0,0);
		occu_select_textView.setLayoutParams(occu_select_params);
		occu_select_textView.setTextSize((20f*ry)/global.f);
		close_button = (Button)findViewById(R.id.close);
		LayoutParams close_Params = new LayoutParams(
				(int)(32*rx),(int)(32*ry));
		close_Params.setMargins((int)(10*rx),0,0,0);
		close_button.setLayoutParams(close_Params);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.occupation_spinner_dialog_view);
		global = new Global();
		rx = global.getRx();
		ry = global.getRy();
		setElements();
//		close_button = (Button)findViewById(R.id.close);
		ListView lv = (ListView)findViewById(R.id.occupationspinnerList);
		lv.setAdapter(new SpinnerDialogArrayAdapter(OccupationSpinnerDialog.this, values));
		lv.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				global.setOccupation_str(values[position]);
				finish();
			}

		});
		close_button.setOnClickListener(close_button_listener);
	}
	public OnClickListener close_button_listener = new OnClickListener() {

		public void onClick(View arg0) {
			finish();
		}
	};
	@Override  
	public void onBackPressed() {
		//Do Nothing
	}
}