package com.dcastalia.foodmenu;

public class Restaurent {
    
    String logo;
    String name;
    String desc;
    String open;
    String close;
    String long_desc;
    public Restaurent(String logo, String name, String desc,String long_desc, String open, String close){
        this.logo = logo;
        this.name = name;
        this.desc = desc;
        this.open = open;
        this.close = close;
        this.long_desc = long_desc;
    }
    
    public String getLogo(){
        return logo;
    }
    
    public String getName(){
        return name;
    }
    
    public String getDesc(){
        return desc;
    }
    
    public String getLong_desc(){
    	return long_desc;
    }
    
    public String getOpen(){
        return open;
    }
    
    public String getClose(){
        return close;
    }
    
    
}
