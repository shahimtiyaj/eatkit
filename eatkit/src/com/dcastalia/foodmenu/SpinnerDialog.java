package com.dcastalia.foodmenu;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SpinnerDialog extends Activity{

	Global global;
	Button close_button,set_button;
	EditText amount_text;
	float rx,ry;
	public void setElements(){
		LinearLayout main_spin_layout = (LinearLayout)findViewById(R.id.main_spin_layout);
		main_spin_layout.setLayoutParams(new LayoutParams((int)(280*rx),(int)(350*ry)));
		LinearLayout top_spin_layout = (LinearLayout)findViewById(R.id.top_layout);
		top_spin_layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,(int)(60*ry)));
		ImageView ic_menu_imageview = (ImageView)findViewById(R.id.ic_menu_imageview);
		ic_menu_imageview.setLayoutParams(new LayoutParams((int)(50*rx),(int)(50*ry)));
		TextView amount_select_textView = (TextView)findViewById(R.id.item_number_textview);
		LayoutParams amount_select_params = new LayoutParams((int)(180*rx),LayoutParams.FILL_PARENT);
//		amount_select_params.setMargins((int)(50*rx),0,0,0);
		amount_select_textView.setLayoutParams(amount_select_params);
		amount_select_textView.setTextSize((20f*ry)/global.f);
		close_button = (Button)findViewById(R.id.close);
		LayoutParams close_Params = new LayoutParams(
				(int)(32*rx),(int)(32*ry));
		close_Params.setMargins((int)(10*rx),0,0,0);
		close_button.setLayoutParams(close_Params);
		LinearLayout spinner_layout = (LinearLayout)findViewById(R.id.spinnerList_layout);
		spinner_layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,(int)(245*ry)));

		RelativeLayout bottom_laLayout = (RelativeLayout)findViewById(R.id.bottom_layout);
		bottom_laLayout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,(int)(42*ry)));
		bottom_laLayout.setPadding((int)(2*rx),0,(int)(2*rx),(int)(2*ry));
		TextView amount_textview = (TextView)findViewById(R.id.amount_textview);
		//amount_textview.setPadding((int)(30*rx), 0, 0, 0);
		RelativeLayout.LayoutParams amount_textview_params = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,(int)(12*ry));
		//amount_textview_params.setMargins((int)(30*rx), 0, 0, 0);
		amount_textview_params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		amount_textview_params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		amount_textview.setLayoutParams(amount_textview_params);
		amount_textview.setTextSize((10f*ry)/global.f);
		amount_textview.setText("A M O U N T");
		set_button = (Button)findViewById(R.id.set_amount_button);
		RelativeLayout.LayoutParams set_params = new RelativeLayout.LayoutParams((int)(75*rx),RelativeLayout.LayoutParams.FILL_PARENT);
		set_params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		set_button.setLayoutParams(set_params);
		amount_text = (EditText)findViewById(R.id.amount_edit_text);
		RelativeLayout.LayoutParams amount_text_params = new RelativeLayout.LayoutParams((int)(200*rx),(int)(23*ry));
		amount_text_params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		amount_text.setLayoutParams(amount_text_params);
		amount_text.setTextSize((16f*ry)/global.f);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.spinner_dialog_view);
		global = new Global();
		rx = global.getRx();
		ry = global.getRy();
		setElements();
	
		
		String values [] = {"1","2","3","4","5","6","7","8","9","10"};
		ListView lv = (ListView)findViewById(R.id.spinnerList);
		lv.setAdapter(new SpinnerDialogArrayAdapter(SpinnerDialog.this, values));
		lv.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				global.setSpinnerposition(position);
				finish();
			}
		});
		close_button.setOnClickListener(close_button_listener);
		set_button.setOnClickListener(set_button_listener);
	}
	public OnClickListener close_button_listener = new OnClickListener() {

		public void onClick(View arg0) {
			finish();
		}
	};
	public OnClickListener set_button_listener = new OnClickListener() {
		
		public void onClick(View v) {
			if(((amount_text.getText()).toString()).equals("")){
				Toast.makeText(SpinnerDialog.this, "Please Select Number of Items", 1).show();
			}else{
				global.setSpinnerposition(Integer.parseInt((amount_text.getText()).toString())-1);
				finish();
			}
		}
	};
	@Override  
	public void onBackPressed() {

	}
}
