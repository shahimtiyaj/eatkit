package com.dcastalia.foodmenu;

import com.loopj.android.image.SmartImageView;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView.ScaleType;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class MenuView extends Activity implements OnClickListener,OnTouchListener{
	String Name;
	AnimationDrawable adAnimation;
	String [] values;
	Random rnd;
	private ListView Lister;
	String menuurl;
	ListCategoryParser Listparser;
	MenuParser Menuparser; 
	Global global;
	TextView MenuText;
	int i;
	String click_text = " ITEMS CHOSEN: ";
	String s = "";
	private AlertDialog alertDialog;
	TextView t;
	private TextView categoryText;
	private TextView Menucategorystr;
	private TextView ItemText;
	private Button checkout;
	private Button Exit;
	private Button Back;
	private OptionsParser OP;
	SmartImageView staticImg;
	SmartImageView logoView;
	ImageView sc_down,sc_up;
	ScrollView menu_scroller;
	TextView open_time, close_time, r_name, r_desc;
	private Timer mTimer;
	private long delayInMili = 5000;
	private int nextImg = 0;
	SmartImageView adview;
	TableLayout mtl;
	TableRow mtr;
	TableLayout ftl;
	TableRow ftr;
	boolean elementadded;
	Builder EXIT_DIALOG;
	int scrollX, scrollY;
	Timer mmTimer;
	float rx,ry;
	//	private TableLayout bckt_table;

	public void setElements(){

		LinearLayout main_layout = (LinearLayout)findViewById(R.id.main_layout);
		LinearLayout.LayoutParams main_layout_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, (int)(390*ry));
		main_layout.setLayoutParams(main_layout_params);
		main_layout.setOrientation(LinearLayout.HORIZONTAL);

		LinearLayout main_layout_left = (LinearLayout)findViewById(R.id.main_layout_left);
		LinearLayout.LayoutParams main_layout_left_params = new LinearLayout.LayoutParams
				((int)(600*rx),LinearLayout.LayoutParams.FILL_PARENT);
		main_layout_left_params.setMargins(0, (int)(5*ry), 0, 0);
		main_layout_left.setLayoutParams(main_layout_left_params);
		main_layout_left.setOrientation(LinearLayout.VERTICAL);

		LinearLayout main_layout_right = (LinearLayout)findViewById(R.id.main_layout_right);
		LinearLayout.LayoutParams main_layout_right_params = new LinearLayout.LayoutParams
				((int)(185*rx),LinearLayout.LayoutParams.FILL_PARENT);
		main_layout_right_params.setMargins((int)(5*rx), (int)(5*ry), 0, (int)(5*ry));
		main_layout_right.setLayoutParams(main_layout_right_params);
		main_layout_right.setOrientation(LinearLayout.VERTICAL);

		LinearLayout bottom_layout = (LinearLayout)findViewById(R.id.bottom_layout);
		LinearLayout.LayoutParams bottom_layout_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, (int)(64*ry));
		bottom_layout.setLayoutParams(bottom_layout_params);
		bottom_layout.setOrientation(LinearLayout.HORIZONTAL);
		logoView = (SmartImageView)findViewById(R.id.logoview);
		logoView.setLayoutParams(new LayoutParams((int)(64*rx),(int)(64*ry)));
		logoView.setScaleType(ScaleType.FIT_XY);
		adview = (SmartImageView)findViewById(R.id.adImageView);
		adview.setOnClickListener(adview_listener);
		adview.setLayoutParams(new LayoutParams((int)(540*rx),(int)(64*ry)));
		adview.setScaleType(ScaleType.FIT_XY);

		menu_scroller = (ScrollView)findViewById(R.id.MenuScrollView);
		menu_scroller.setOnTouchListener(this);
		LayoutParams menu_scroller_params = new LayoutParams(LayoutParams.FILL_PARENT,
				(int)(340*ry));
		menu_scroller_params.setMargins((int)(10*rx), 0, 0, 0);
		menu_scroller.setLayoutParams(menu_scroller_params);
		menu_scroller.setVerticalFadingEdgeEnabled(false);

		checkout = (Button)this.findViewById(R.id.Checkout);
		checkout.setOnClickListener(checkout_listener);

		LayoutParams checkout_params = new LayoutParams((int)(90*rx),(int)(64*ry));
		checkout_params.setMargins(0, 0, (int)(5*rx), 0);
		checkout.setLayoutParams(checkout_params);

		Exit = (Button)this.findViewById(R.id.Exit);
		Exit.setOnClickListener(EXIT_APPLICATION);

		LayoutParams exit_params = new LayoutParams((int)(90*rx),(int)(64*ry));
		exit_params.setMargins(0, 0, (int)(5*rx), 0);
		Exit.setLayoutParams(exit_params);
		Exit.setTextSize((16f*(ry-.1f))/global.f);
		LinearLayout list_layout = (LinearLayout)findViewById(R.id.list_layout);
		LinearLayout.LayoutParams list_layout_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, (int)(320*ry));
		list_layout_params.setMargins(0, 0, 0, (int)(2*ry));
		list_layout.setLayoutParams(list_layout_params);

		Back = (Button)findViewById(R.id.BackButton);
		Back.setOnClickListener(Back_Button_Listener);
		LayoutParams back_params = new LayoutParams(LayoutParams.FILL_PARENT,
				(int)((45*ry)/global.f));
		back_params.setMargins((int)(1.5*rx), 0, (int)(1.5*rx), 0);
		//back_params.setMargins(left, top, right, bottom);
		Back.setLayoutParams(back_params);

		categoryText = (TextView)this.findViewById(R.id.categoryNameText);
		categoryText.setOnClickListener(categoryText_click_listener);
		categoryText.setTextSize((18f*ry)/global.f);
		MenuText = (TextView)this.findViewById(R.id.MenuNameText);
		MenuText.setTextSize((18f*ry)/global.f);
		ItemText = (TextView)this.findViewById(R.id.ItemNameText);
		ItemText.setTextSize((18f*ry)/global.f);
		Menucategorystr = (TextView)findViewById(R.id.mcatstr);
		Menucategorystr.setOnClickListener(categoryText_click_listener);
		Menucategorystr.setTextSize((18f*ry)/global.f);
		sc_down = (ImageView)findViewById(R.id.menu_scroll_down);
		sc_up = (ImageView)findViewById(R.id.menu_scroll_up);
		LayoutParams sc_down_params = new LayoutParams(LayoutParams.FILL_PARENT,(int)(10*ry));
		LayoutParams sc_up_params = new LayoutParams(LayoutParams.FILL_PARENT,(int)(10*ry));
		sc_down_params.setMargins((int)(15*rx),0,0,0);
		sc_up_params.setMargins((int)(15*rx),0,0,0);
		sc_down.setScaleType(ScaleType.FIT_XY);
		sc_up.setScaleType(ScaleType.FIT_XY);
		sc_down.setLayoutParams(sc_down_params);
		sc_up.setLayoutParams(sc_up_params);
		TableRow cat_text_row = (TableRow)findViewById(R.id.categoryTextview_layout);
		TableRow.LayoutParams cat_text_row_params = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
		cat_text_row_params.setMargins((int)(20*rx),0,0,0);
		cat_text_row.setLayoutParams(cat_text_row_params);

	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);        
		setContentView(R.layout.menu);
		global = new Global();
		rx = global.getRx();
		ry = global.getRy();
		Lister = (ListView)findViewById(R.id.List);	
		setElements();
		mtl = (TableLayout)findViewById(R.id.MenuTable);
		EXIT_DIALOG = new AlertDialog.Builder(this);

		new Loading().execute();
	}
	public void setListerItems(){

		MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(this, values);
		//		setListAdapter(adapter);
		Lister.setAdapter(adapter);
		Lister.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				if(global.getListId()!=position){
					global.setListId(position);
					startActivity(new Intent("MenuView"));
					finish();
				}
			}
		});		
	}

	public void MenuViewGen(){
		try{
			if(Menuparser.mlist.size()>0){
				i=0;
				elementadded = false;
				for(i=0;i<Menuparser.mlist.size();i++){
					if(i%2==0){
						//Log.d("TAG","new row");
						mtr = new TableRow(this);
						mtr.setLayoutParams(new LayoutParams(
								LayoutParams.FILL_PARENT,
								LayoutParams.WRAP_CONTENT));
						elementadded = false;
					}

					mtr.addView(MenuButton(Menuparser.mlist.get(i).id,Menuparser.mlist.get(i).name,Menuparser.mlist.get(i).image,
							Menuparser.mlist.get(i).desc,Menuparser.mlist.get(i).item_count));
					if(i%2==1){
						elementadded = true;
						mtl.addView(mtr,new TableLayout.LayoutParams(
								LayoutParams.FILL_PARENT,
								LayoutParams.WRAP_CONTENT));
					}
				}
				if(!elementadded)
					mtl.addView(mtr,new TableLayout.LayoutParams(
							LayoutParams.FILL_PARENT,
							LayoutParams.WRAP_CONTENT));

			}else{
				alertDialog = new AlertDialog.Builder(this).create();
				alertDialog.setTitle("Sorry");  
				alertDialog.setMessage("Nothing under this category");  
				alertDialog.setButton("OK", new DialogInterface.OnClickListener() {  
					public void onClick(DialogInterface dialog, int which) {  
						finish();
						return;  
					} });   
				alertDialog.show();

			}
		}catch(Exception e){

		}
	}
	public LinearLayout MenuButton(String id, String name, String img2, String desc, String item_count){

		LinearLayout parent = new LinearLayout(this);     
		parent.setBackgroundResource(R.drawable.button_background);
		parent.setOrientation(LinearLayout.HORIZONTAL);
		parent.setPadding((int)(18*rx), (int)(12*ry), (int)(6*rx), (int)(2*ry));

		parent.setClickable(true);
		parent.setOnClickListener(menu_listener);
		parent.setId(Integer.parseInt(id));
		parent.setLayoutParams(new LayoutParams(
				(int)(300*rx),
				(int)(100*ry)));


		LinearLayout child1 = new LinearLayout(this);
		SmartImageView imgView = new SmartImageView(this);

		imgView.setImageUrl(img2);
		imgView.setLayoutParams(new LayoutParams((int)(75*rx),(int)(75*ry)));
		imgView.setScaleType(ScaleType.FIT_XY);
		child1.setLayoutParams(new LayoutParams(
				LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
		child1.addView(imgView);
		child1.setPadding((int)(0*rx), (int)(0*ry), (int)(10*rx), (int)(0*ry));

		parent.addView(child1);

		LinearLayout child2 = new LinearLayout(this);
		child2.setOrientation(LinearLayout.VERTICAL);
		//		child2.setPadding(0, 3, 0, 3);
		child2.setLayoutParams(new LayoutParams(
				LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));

		TextView text1 = new TextView(this);
		text1.setLayoutParams(new LayoutParams((int)(190*rx),(int)(25*ry)));
		text1.setTextSize((15f*ry)/global.f);
		text1.setTextColor(android.graphics.Color.BLACK);
		text1.setTypeface(null, android.graphics.Typeface.BOLD);
		text1.setText(name);
		//ScrollView scview = new ScrollView(this);
		//scview.setLayoutParams(new LayoutParams(150,30));
		TextView text2 = new TextView(this);
		text2.setLayoutParams(new LayoutParams((int)(190*rx),(int)(25*ry)));
		text2.setText(desc);
		text2.setTextColor(android.graphics.Color.DKGRAY);
		text2.setTypeface(null, android.graphics.Typeface.BOLD);
		text2.setTextSize((10f*ry)/global.f);
		//scview.addView(text2);
		child2.addView(text1);
		child2.addView(text2);


		LinearLayout child3 = new LinearLayout(this);
		child3.setBackgroundResource(R.drawable.add_2_bckt);
		LinearLayout.LayoutParams lp = new LayoutParams((int)(80*rx), (int)(20*ry));
		child3.setLayoutParams(lp);
		lp.setMargins(0,(int)(4*ry), 0, 0);
		//child3.setPadding((int)(0*rx), (int)(30*ry), (int)(0*rx), (int)(0*ry));
		//child3.setPadding(left, top, right, bottom)

		LinearLayout child4 = new LinearLayout(this);
		child4.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));

		TextView text3 = new TextView(this);
		text3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
		text3.setText(item_count+" items");
		text3.setTextColor(android.graphics.Color.WHITE);
		text3.setTypeface(null,android.graphics.Typeface.BOLD);
		text3.setTextSize((12f*ry)/global.f);
		child3.setPadding((int)(10*rx), 0, 0, 0);
		child3.addView(text3);
		child4.addView(child3);
		child4.setPadding(0, 0, (int)(4*rx), 0);
		child4.setGravity(Gravity.RIGHT|Gravity.BOTTOM);
		child2.addView(child4);
		parent.addView(child2);

		return parent;
	}

	public void Menureader(String url){

		try{
			URL website = new URL(url);

			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();
			Menuparser = new MenuParser();
			xr.setContentHandler(Menuparser);
			xr.parse(new InputSource(website.openStream()));
			createFile(url);


		}catch (Exception e) {
			Log.d("TAG", "MenuReaderError! "+e);

		}
	}


	public void MenureaderOff(File file){

		try{

			//URL website = new URL(url);

			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();
			Menuparser = new MenuParser();
			//global.setOptionsparser();
			xr.setContentHandler(Menuparser);
			//xr.parse(new InputSource(website.openStream()));
			//xr.parse(new InputSource(getResources().openRawResource(id)));
			InputStream in = new BufferedInputStream(new FileInputStream(file));
			xr.parse(new InputSource(in));

		}catch (Exception e) {
			Log.d("TAG", "error! "+e);
		}
	}
	
	public void createFile(String url) throws IOException{
		URL url1 = new URL(url);

		//create the new connection  

		HttpURLConnection urlConnection = (HttpURLConnection) url1.openConnection();

		//set up some things on the connection

		urlConnection.setRequestMethod("GET");

		urlConnection.setDoOutput(true);

		//and connect!

		urlConnection.connect();

		//set the path where we want to save the file

		//in this case, going to save it on the root directory of the

		//sd card.
		File sdCard = Environment.getExternalStorageDirectory();
		File dir = new File (sdCard.getAbsolutePath());
		dir.mkdirs();
		

		//File SDCardRoot = new File("/sdcard/"+"Some Folder Name/");

		//create a new file, specifying the path, and the filename

		//which we want to save the file as.

		File file = new File(dir,Name);
		Log.w("myApp", "Data Storing");

		//this will be used to write the downloaded data into the file we created

		FileOutputStream fileOutput = new FileOutputStream(file);

		//this will be used in reading the data from the internet

		InputStream inputStream = urlConnection.getInputStream();

		//this is the total size of the file

		int totalSize = urlConnection.getContentLength();

		//variable to store total downloaded bytes

		int downloadedSize = 0;

		//create a buffer...

		byte[] buffer = new byte[1024];

		int bufferLength = 0; //used to store a temporary size of the buffer

		//now, read through the input buffer and write the contents to the file

		while ( (bufferLength = inputStream.read(buffer)) > 0 ) 

		{

		//add the data in the buffer to the file in the file output stream (the file on the sd card

		fileOutput.write(buffer, 0, bufferLength);

		//add up the size so we know how much is downloaded

		downloadedSize += bufferLength;

		int progress=(int)(downloadedSize*100/totalSize);

		//this is where you would do something to report the prgress, like this maybe

		//updateProgress(downloadedSize, totalSize);

		}

		//close the output stream when done

		fileOutput.close();
	}
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if(hasFocus){
			try{
				if((global.getOptionsparser()).equals(null))
					System.exit(0);
			}catch(Exception e){
				System.exit(0);
			}
		}
	}
	public OnClickListener menu_listener = new OnClickListener() {

		public void onClick(View v) {
			global.setFeaturedItemClicked(false);
			global.setMenuId(v.getId());
			global.setMenuparser(Menuparser);
			global.setMenu_ctx(MenuView.this);
			startActivity(new Intent("ItemView").addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));	
		}
	};

	public void TobaccoViewGen(){
		try{
			global.setMenuId(0);
			global.setMenuparser(Menuparser);
			global.setMenu_ctx(MenuView.this);

			startActivity(new Intent("ItemView").addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
		}catch(Exception e){

		}
	}
	public void onClick(View v) {
		try{
		}catch(Exception e){
			Log.d("TAG",e.toString());
		}
	}

	public OnClickListener EXIT_APPLICATION = new OnClickListener() {

		public void onClick(View v) {
			EXIT_DIALOG
			.setTitle("EXIT APPLICATION")  
			.setMessage("Do you really want to exit?")  
			.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					try{
						System.runFinalizersOnExit(true);
						System.exit(0);
					}catch(Exception e){

					}
				}
			})
			.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					return;
				}
			}).show();			
		}
	};

	public OnClickListener Back_Button_Listener = new OnClickListener() {
		public void onClick(View arg0) {
			finish();
		}
	};

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}

	public OnClickListener checkout_listener = new OnClickListener() {

		public void onClick(View v) {
			try{
				if(global.getCount()>0||global.getTobaccoList().size()>0)
					startActivity(new Intent("ConfWin"));
				else
					Toast.makeText(MenuView.this, "Please select an item", 5).show();
			}catch(Exception x){
				Log.d("TAG",x.toString());
			}
		}
	};
	public OnClickListener categoryText_click_listener = new OnClickListener(){
		public void onClick(View arg0) {
			finish();
		}
	};

	@Override
	protected void onPause() {
		super.onPause();
		if(mTimer!=null){
			mTimer.cancel();
			mTimer.purge();
		}
	}
	@Override
	protected void onResume() {
		super.onResume();
		mTimer = new Timer();
		mTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				adview.post(new Runnable() {
					public void run() {
						try{
							adview.setImageUrl(OP.ads.get((nextImg = nextImg%OP.ads.size())).image);
							nextImg+=1;                    		
						}catch(Exception e){
						}
					}
				});
			}
		},0,delayInMili);
		mmTimer = new Timer();
		mmTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				menu_scroller.post(new Runnable() {
					public void run() {
						try{
							if(menu_scroller != null){
								scrollX = menu_scroller.getScrollX();
								scrollY = menu_scroller.getScrollY();
							}
							if(mtl!=null && mtl.getChildCount()>3){
								if(scrollY == 0){
									sc_up.setImageResource(R.drawable.arrow_background_1);
									sc_down.setImageResource(R.drawable.arrow_down_1);
								}
								else if((scrollY+ (int)(100*ry*3) + 50*ry) > mtl.getHeight()){
									sc_up.setImageResource(R.drawable.arrow_up_1);
									sc_down.setImageResource(R.drawable.arrow_background_1);
								}
							}
						}catch(Exception e){
						}
					}
				});
			}
		},0,100);
	}
	public OnClickListener stad_listener = new OnClickListener(){
		public void onClick(View v) {
			try{
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				intent.addCategory(Intent.CATEGORY_BROWSABLE);
				intent.setData(Uri.parse(OP.stad.link));
				startActivity(intent);
			}catch(Exception x){
			}
		}
	};
	public OnClickListener adview_listener = new OnClickListener(){
		public void onClick(View v) {
			String link = OP.ads.get(((nextImg-1)%OP.ads.size())).link;
			if(!link.startsWith("http"))
				link = "http://"+link;
			try{
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				intent.addCategory(Intent.CATEGORY_BROWSABLE);
				intent.setData(Uri.parse(link));
				startActivity(intent);

			}catch(Exception x){
			}
		}
	};
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		//		Log.v("ON_TOUCH", "Touched");
		return super.onTouchEvent(event);
	}
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		//		Log.v("ON_TOUCH", "Action = " + ev.getAction());
		return super.dispatchTouchEvent(ev);
	}
	boolean toggle = true;
	public boolean onTouch(View v, MotionEvent event) {
		//		int action = event.getAction();
		//		float x = event.getX();
		//		float y = event.getY();
		if(mtl!=null){

			if(mtl!=null && mtl.getChildCount()>3){
				if(scrollY == 0){
					sc_up.setImageResource(R.drawable.arrow_background_1);
					sc_down.setImageResource(R.drawable.arrow_down_1);

				}
				else if((scrollY+ (int)(100*ry*3) + 50*ry) >= mtl.getHeight()){
					sc_up.setImageResource(R.drawable.arrow_up_1);
					sc_down.setImageResource(R.drawable.arrow_background_1);
				}else{
					sc_up.setImageResource(R.drawable.arrow_up_1);
					sc_down.setImageResource(R.drawable.arrow_down_1);
				}
			}
		}
		return false;
	}
	class Loading extends AsyncTask<String, Void, Void>{
		ProgressDialog dialog = new ProgressDialog(MenuView.this);
		@Override
		protected Void doInBackground(String... params) {
			if(global.isOnline){
				try{
					OP = global.getOptionsparser();
					Listparser = global.getListparser();
					rnd = new Random();
					Name = Listparser.list.get(global.getListId()).name;
					Menureader(menuurl = (Listparser.list.get(global.getListId()).fileName+"?p="
							+rnd.nextInt()));
					
				}catch(Exception e){
					Log.e("ERROR",e.toString());
				}
			}
			else{
				OP = global.getOptionsparser();
				Listparser = global.getListparser();
				Name = Listparser.list.get(global.getListId()).name;
				
				File sdCard = Environment.getExternalStorageDirectory();
				File dir = new File (sdCard.getAbsolutePath());
				File file = new File(dir,Name);
				MenureaderOff(file);
				
				/*if(global.getListId()==0){
					MenureaderOff(R.raw.starter);
				}
				else if(global.getListId()==1){
					MenureaderOff(R.raw.main_dish);
				}
				else if(global.getListId()==2){
					MenureaderOff(R.raw.dessert);
				}
				else if(global.getListId()==3){
					MenureaderOff(R.raw.vegetarian);
				}*/
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try{
				adview.setImageUrl(OP.ads.get((nextImg = nextImg%OP.ads.size())).image);
				logoView.setImageUrl(OP.restaurent.logo);
				ItemText.setText("");
				categoryText.setText("EatKit Category >> ");
				MenuText.setText(Listparser.list.get(global.getListId()).getName()+" Items >> ");
				if((Menuparser.mlist.get(0).name).equals("BENSON & HEDGES")){
					//TobaccoViewGen();
				}else
					MenuViewGen();
				values = new String[Listparser.list.size()];
				for(int i=0;i<values.length;i++){
					values[i] = Listparser.list.get(i).name;
				}
				setListerItems();
			}catch(Exception e){
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			this.dialog.setMessage("Fetching data...");
			this.dialog.show();	

		}

	}
}
