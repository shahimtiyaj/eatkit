package com.dcastalia.foodmenu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.image.SmartImageView;

public class AfterLoginScreen extends Activity{

	private int PicPosition;
	private Handler handler = new Handler();

	Button home_button;
	Button logout_button;
	Button exit_button;
	Global global;
	OptionsParser OP;
	TextView r_name;
	TextView r_desc;
	TextView start_time;
	TextView end_time;
	TextView long_desc;
	Gallery gallery;
	int i;
	float rx,ry;
	ImageAdapter iv;
	String restphoto[];
	Builder EXIT_DIALOG;
	public void setElements(){
		LinearLayout back_layout = (LinearLayout)findViewById(R.id.background_id);
		LinearLayout.LayoutParams back_layout_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,LinearLayout.LayoutParams.FILL_PARENT);
		back_layout_params.setMargins((int)(10*rx),(int)(10*ry),(int)(10*rx),(int)(10*ry));
		back_layout.setLayoutParams(back_layout_params);
		LinearLayout head_layout = (LinearLayout)findViewById(R.id.head_id);
		LinearLayout.LayoutParams head_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,(int)(66*ry));
		head_params.setMargins((int)(20*rx),(int)(10*ry),(int)(0*rx),(int)(20*ry));
		head_layout.setLayoutParams(head_params);
		LinearLayout namenlogo_layout = (LinearLayout)findViewById(R.id.name_logo_layout);
		LinearLayout.LayoutParams namenlogo_params = new LinearLayout.LayoutParams(
				(int)(600*rx),LinearLayout.LayoutParams.FILL_PARENT);
		namenlogo_params.gravity = Gravity.LEFT;
		namenlogo_layout.setLayoutParams(namenlogo_params);

		SmartImageView logo = (SmartImageView)findViewById(R.id.logoview);
		logo.setLayoutParams(new LinearLayout.LayoutParams(
				(int)(64*rx),(int)(64*ry)));
		logo.setScaleType(ScaleType.FIT_XY);
		logo.setImageUrl(OP.restaurent.logo);

		r_name = (TextView)findViewById(R.id.r_name);
		LinearLayout.LayoutParams r_name_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,(int)(32*ry));
		r_name_params.gravity = Gravity.CENTER;
		r_name.setLayoutParams(r_name_params);
		r_name.setTextSize((26f*ry)/global.f);
		r_name.setText(OP.restaurent.getName());

		r_desc = (TextView)findViewById(R.id.r_desc);
		LinearLayout.LayoutParams r_desc_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,(int)(32*ry));
		r_desc_params.gravity = Gravity.CENTER;
		r_desc.setLayoutParams(r_desc_params);
		r_desc.setTextSize((18f*ry)/global.f);
		r_desc.setText(OP.restaurent.getDesc());

		LinearLayout r_time_layout = (LinearLayout)findViewById(R.id.res_time_layout);
		LinearLayout.LayoutParams r_time_params = new LinearLayout.LayoutParams(
				(int)(100*rx),(int)(66*ry));
		r_time_params.gravity = Gravity.LEFT;
		r_time_params.setMargins((int)(20*rx),0,0,(int)(30*ry));
		r_time_layout.setLayoutParams(r_time_params);
		
		start_time = (TextView)findViewById(R.id.start_time);
		start_time.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,(int)(32*ry)));
		start_time.setTextSize((14f*ry)/global.f);
		
		end_time = (TextView)findViewById(R.id.end_time);
		end_time.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,(int)(32*ry)));
		end_time.setTextSize((14f*ry)/global.f);
		
		LinearLayout mid_layout = (LinearLayout)findViewById(R.id.mid_layout);
		LinearLayout.LayoutParams mid_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		mid_layout.setLayoutParams(mid_params);
//		mid_layout.setBackgroundColor(android.graphics.Color.RED);
		LinearLayout gal_layout = (LinearLayout)findViewById(R.id.gallery_layout);
		LinearLayout.LayoutParams gal_params = new LinearLayout.LayoutParams(
				(int)(600*rx),(int)(200*ry));
		gal_params.setMargins((int)(20*rx),(int)(0*ry),(int)(0*rx),(int)(30*ry));
//		gal_params.gravity = Gravity.LEFT;
		gal_layout.setLayoutParams(gal_params);
		
		LinearLayout button_layout = (LinearLayout)findViewById(R.id.button_layout);
		LinearLayout.LayoutParams button_layout_params = new LinearLayout.LayoutParams(
				(int)(100*rx),(int)(200*ry));
		button_layout_params.setMargins((int)(20*rx),(int)(0*ry),(int)(0*rx),(int)(30*ry));
		button_layout_params.gravity = Gravity.LEFT;
		button_layout.setLayoutParams(button_layout_params);
		
		home_button = (Button)findViewById(R.id.home_button);
		home_button.setOnClickListener(home_button_listener);
		LinearLayout.LayoutParams home_button_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,(int)(60*ry));
		home_button_params.setMargins(0, 0, 0, (int)(5*ry));
		home_button.setLayoutParams(home_button_params);
		home_button.setTextSize((20f*ry)/global.f);
		
		
		logout_button = (Button)findViewById(R.id.logout_button);
		logout_button.setOnClickListener(logout_button_listener);
		LinearLayout.LayoutParams logout_button_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,(int)(60*ry));
		logout_button_params.setMargins(0, 0, 0, (int)(5*ry));
		logout_button.setLayoutParams(logout_button_params);
		logout_button.setTextSize((20f*ry)/global.f);
		
		exit_button = (Button)findViewById(R.id.exit_button);
		exit_button.setOnClickListener(EXIT_APPLICATION);
		LinearLayout.LayoutParams exit_button_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,(int)(60*ry));
//		exit_button_params.setMargins(0, 0, 0, (int)(10*ry));
		exit_button.setLayoutParams(exit_button_params);
		exit_button.setTextSize((20f*ry)/global.f);
		
		long_desc = (TextView)findViewById(R.id.long_desc);
		LinearLayout.LayoutParams long_desc_params = new LinearLayout.LayoutParams(
				(int)(700*rx),(int)(50*ry));
		long_desc_params.setMargins((int)(20*rx),0,0,0);
		long_desc.setLayoutParams(long_desc_params);
		long_desc.setTextSize((18f*ry)/global.f);
//		long_desc.setBackgroundColor(android.graphics.Color.RED);
		
		//ImageView ordereat_logo = (ImageView)findViewById(R.id.ordereatlogo);
		RelativeLayout.LayoutParams ordereat_params = new RelativeLayout.LayoutParams(
				(int)(100*rx),(int)(50*ry));
		ordereat_params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		ordereat_params.setMargins((int)(20*rx),0,0,0);
		//ordereat_logo.setLayoutParams(ordereat_params);
//		ordereat_logo.setImageResource(R.drawable.order_eat_logo);
//		ordereat_logo.setScaleType(ScaleType.FIT_XY);
		
		ImageView ferra_logo = (ImageView)findViewById(R.id.ferratechlogo);
		RelativeLayout.LayoutParams ferra_params = new RelativeLayout.LayoutParams(
				(int)(100*rx),(int)(30*ry));
		ferra_params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		ferra_params.setMargins(0,0,(int)(40*rx),0);
		ferra_logo.setLayoutParams(ferra_params);
		
		TextView pwr_txt = (TextView)findViewById(R.id.powered_text);
		RelativeLayout.LayoutParams pwr_params = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
		pwr_params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		pwr_params.setMargins(0,(int)(15*ry),(int)(130*rx),0);
		pwr_txt.setLayoutParams(pwr_params);
		pwr_txt.setTextSize((12f*ry)/global.f);
		
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.after_login_screen);

		global = new Global();
		rx = global.getRx();
		ry = global.getRy();
		OP = global.getOptionsparser();
		setElements();

		EXIT_DIALOG = new AlertDialog.Builder(this);

		
		long_desc.setText(OP.restaurent.getLong_desc());

		
		if(!OP.restaurent.getOpen().equals(""))
			start_time.setText("From:"+OP.restaurent.getOpen());

		
		if(!OP.restaurent.getClose().equals(""))
			end_time.setText("To:   "+OP.restaurent.getClose());

		restphoto = new String[OP.rest_photo.size()];
		for(int i=0;i<restphoto.length;i++){
			restphoto[i] = OP.rest_photo.get(i).getImage_Url();
			//			Log.d("TAG",restphoto[i]);
		}

		gallery = (Gallery) findViewById(R.id.gallery1);
		gallery.setAdapter(new ImageAdapter(this));
		if(restphoto.length>1){
			gallery.setSelection(1);
			runnable.run();
		}

	}
	private Runnable runnable = new Runnable() {
		public void run() {
			myslideshow();
			handler.postDelayed(this, 1500);
		}
	};

	private void myslideshow()
	{
		PicPosition = gallery.getSelectedItemPosition() +1;             
		if (PicPosition >=  restphoto.length-1){            
			PicPosition =  1;
			gallery.setSelection(PicPosition);
		}
		else{
			gallery.setSelection(PicPosition);
		}
	}
	OnClickListener home_button_listener = new OnClickListener() {

		public void onClick(View v) {
			startActivity(new Intent("ListCategoryView"));
			finish();
		}
	};
	
	
	OnClickListener logout_button_listener = new OnClickListener() {

		public void onClick(View v) {
			Intent i = new Intent(AfterLoginScreen.this,FoodMenu.class);
			startActivity(i);
			finish();
		}
	};
	
	
	public OnClickListener EXIT_APPLICATION = new OnClickListener() {

		public void onClick(View v) {
			EXIT_DIALOG
			.setTitle("EXIT APPLICATION")  
			.setMessage("Do you really want to exit?")  
			.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					try{
						System.runFinalizersOnExit(true);
						System.exit(0);
						//android.os.Process.killProcess(android.os.Process.myPid());
					}catch(Exception e){

					}
				}
			})
			.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					//do stuff onclick of YES
					return;
					//finish();
				}
			}).show();
		}
	};
	public class ImageAdapter extends BaseAdapter 
	{
		private Context context;
		private int itemBackground;

		public ImageAdapter(Context c) 
		{
			context = c;
			//---setting the style---
			TypedArray a = obtainStyledAttributes(R.styleable.Gallery1);
			itemBackground = a.getResourceId(
					R.styleable.Gallery1_android_galleryItemBackground, 0);
			a.recycle();                    
		}

		//---returns the number of images---
		public int getCount() {
			return restphoto.length;
		}

		//---returns the ID of an item--- 
		public Object getItem(int position) {
			return position;
		}            

		public long getItemId(int position) {
			return position;
		}

		//---returns an ImageView view---
		public View getView(int position, View convertView, ViewGroup parent) {
			SmartImageView imageView = new SmartImageView(context);
			imageView.setImageUrl(restphoto[position]);
			imageView.setScaleType(SmartImageView.ScaleType.FIT_XY);
			imageView.setLayoutParams(new Gallery.LayoutParams(
					(int)(200*rx), (int)(200*ry)));
			imageView.setBackgroundResource(itemBackground);
			return imageView;
		}
	}    
}