package com.dcastalia.foodmenu;

public class RestPhoto {

	String ImageUrl;
	
	public RestPhoto(String url){
		this.ImageUrl = url;
	}
	
	public String getImage_Url(){
		return ImageUrl;
	}
}
