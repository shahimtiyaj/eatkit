package com.dcastalia.foodmenu;

public class Tobacco {

	int id_food;
	String food_name;
	int id_cat;
	int amount;
	String desc;
	Double TotalItemPrice;//subitem incl
	Double FoodPrice;
	Double TotalPrice;//amount*totalitemprice
	int rowId;
	public Tobacco(int rowId,int id_food, String food_name, int id_cat, int amount, String desc, 
			Double TotalItemPrice, Double FoodPrice, Double TotalPrice){
		this.rowId = rowId;
		this.id_food = id_food;
		this.food_name = food_name;
		this.id_cat = id_cat;
		this.amount = amount;
		this.desc = desc;
		this.TotalItemPrice = TotalItemPrice;
		this.FoodPrice = FoodPrice;
		this.TotalPrice = TotalPrice;
	}
	
}
