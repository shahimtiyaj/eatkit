package com.dcastalia.foodmenu;

import java.util.ArrayList;

public class Menu {
	String id;
	String name;
	String image;
	String desc;
	String item_count;
	ArrayList<Item> item = new ArrayList<Item>();
	public Menu(String id, String name, String image, String desc, String item_count){
		this.id = id;
		this.name = name;
		this.image = image;
		this.desc = desc;
		this.item_count = item_count;
	}
	
	public void setItem(String id, String name, String image, String desc, String price, String unit){
		item.add(new Item(id,name,image, desc, price, unit));
	}
	
	public String getId(){
		return id;
	}

	public String getName(){
		return name;
	}

	public String getImage(){
		return image;
	}

	public String getDesc(){
		return desc;
	}

	public String getItem_count(){
		return item_count;
	}
}
