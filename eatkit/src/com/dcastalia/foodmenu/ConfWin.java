package com.dcastalia.foodmenu;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public class ConfWin extends Activity implements OnClickListener{

	private AlertDialog alertDialog;
	Global global;
	float rx,ry;
	Button post_button;
	Button clear_button;
	Button cancel_button;
	private int mHour;
	private int mMinute;
	String POST_String;
	private DataHelper dh;
	TableLayout Conftable;
	TableLayout tc_Conftable;
	ArrayList<CheckBox>box;
	ArrayList<TextView>amount_view;
	ArrayList<TextView>total_view;
	ArrayList<LinearLayout>plus_layout;
	ArrayList<LinearLayout>min_layout;
	ArrayList<CheckBox>tc_box;
	ArrayList<TextView>tc_amount_view;
	ArrayList<TextView>tc_total_view;
	ArrayList<LinearLayout>tc_plus_layout;
	ArrayList<LinearLayout>tc_min_layout;

	double totaltotal;
	Builder dialog;
	String temp[];
	List<String>table;
	TextView totalView;
	//TextView tc_txt;
	//TextView tc;
	TextView vtid;
	TextView chid;
	TextView gid;
	TextView totalTK;
	DecimalFormat df;
	public void setElements(){
		LinearLayout confirmation_header = (LinearLayout)findViewById(R.id.confirmation_header_layout);
		LinearLayout.LayoutParams header_params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,(int)(36*ry));
		header_params.setMargins((int)(5*rx), (int)(5*ry), (int)(5*rx), (int)(5*ry));
		confirmation_header.setLayoutParams(header_params);

		TextView remove_header = (TextView)findViewById(R.id.remove_header);
		LinearLayout.LayoutParams remove_header_params =new LinearLayout.LayoutParams((int)(60*rx),LinearLayout.LayoutParams.FILL_PARENT); 
		//remove_header_params.setMargins((int)(5*rx), 0, 0, 0);
		remove_header.setLayoutParams(remove_header_params);
		remove_header.setTextSize((12f*ry)/global.f);
		TextView name_header = (TextView)findViewById(R.id.name_header);
		LinearLayout.LayoutParams name_header_params =new LinearLayout.LayoutParams((int)(350*rx),LinearLayout.LayoutParams.FILL_PARENT);
		name_header_params.setMargins((int)(5*rx), 0, 0, 0);
		name_header.setLayoutParams(name_header_params);
		name_header.setTextSize((14f*ry)/global.f);
		TextView unit_header = (TextView)findViewById(R.id.unit_header);
		LinearLayout.LayoutParams unit_header_params =new LinearLayout.LayoutParams((int)(150*rx),LinearLayout.LayoutParams.FILL_PARENT);
		unit_header_params.setMargins((int)(5*rx), 0, 0, 0);
		unit_header.setLayoutParams(unit_header_params);
		unit_header.setTextSize((14f*ry)/global.f);
		TextView amount_header = (TextView)findViewById(R.id.amount_header);
		LinearLayout.LayoutParams amount_header_params =new LinearLayout.LayoutParams((int)(100*rx),LinearLayout.LayoutParams.FILL_PARENT);
		amount_header_params.setMargins((int)(5*rx), 0, 0, 0);
		amount_header.setLayoutParams(amount_header_params);
		amount_header.setTextSize((14f*ry)/global.f);
		TextView total_header = (TextView)findViewById(R.id.total_header);
		LinearLayout.LayoutParams total_header_params =new LinearLayout.LayoutParams((int)(125*rx),LinearLayout.LayoutParams.FILL_PARENT);
		total_header_params.setMargins((int)(5*rx), 0, 0, 0);
		total_header.setLayoutParams(total_header_params);
		total_header.setTextSize((14f*ry)/global.f);
		ScrollView confirmation_table_scroller = (ScrollView)findViewById(R.id.confirmationtable_scroller);
		LinearLayout.LayoutParams cft_tbl_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,(int)(240*ry));
		cft_tbl_params.setMargins((int)(5*rx),(int)(5*ry),(int)(5*rx),(int)(5*ry));
		confirmation_table_scroller.setLayoutParams(cft_tbl_params);
		//		LinearLayout tobacco_header = (LinearLayout)findViewById(R.id.tobacco_layout);    
		LinearLayout.LayoutParams tobacco_header_params = new LinearLayout.LayoutParams(
				(int)(450*rx),LinearLayout.LayoutParams.FILL_PARENT);
		tobacco_header_params.setMargins((int)(10*rx), 0, (int)(10*rx), (int)(5*ry));
		//		tobacco_header.setLayoutParams(tobacco_header_params);      

		//		TextView tobacco_item_textview = (TextView)findViewById(R.id.tobacco_item_textview);
		LinearLayout.LayoutParams tobacco_item_params = new LinearLayout.LayoutParams(
				(int)(205*rx),LinearLayout.LayoutParams.FILL_PARENT);

		//		tobacco_item_textview.setLayoutParams(tobacco_item_params);
		//		tobacco_item_textview.setTextSize((14f*ry)/global.f);
		//		TextView tobacco_amount_textView = (TextView)findViewById(R.id.tobacco_amunt_textView);
		LinearLayout.LayoutParams tobacco_amount_params = new LinearLayout.LayoutParams(
				(int)(100*rx),LinearLayout.LayoutParams.FILL_PARENT);
		tobacco_amount_params.setMargins((int)(5*rx), 0, 0, 0);
		//		tobacco_amount_textView.setLayoutParams(tobacco_amount_params);
		//		tobacco_amount_textView.setTextSize((14f*ry)/global.f);
		//		TextView tobacco_total_textview = (TextView)findViewById(R.id.tobacco_total_textview);
		LinearLayout.LayoutParams tobacco_total_params = new LinearLayout.LayoutParams(
				(int)(145*rx),LinearLayout.LayoutParams.FILL_PARENT);
		tobacco_total_params.setMargins((int)(5*rx), 0, 0, 0);
		//		tobacco_total_textview.setLayoutParams(tobacco_total_params);
		//		tobacco_total_textview.setTextSize((14f*ry)/global.f);

		LinearLayout res_layout = (LinearLayout)findViewById(R.id.total_vat_tax_panel);
		LinearLayout.LayoutParams vat_tax_params = new LinearLayout.LayoutParams(
				(int)(325*rx),LinearLayout.LayoutParams.FILL_PARENT);
		res_layout.setLayoutParams(vat_tax_params);


		LinearLayout vat_layout = (LinearLayout)findViewById(R.id.vat_layout);
		LinearLayout.LayoutParams vat_layout_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		vat_layout_params.setMargins(0, 0, (int)(5*rx), (int)(5*ry));
		vat_layout_params.gravity = Gravity.RIGHT;
		vat_layout.setLayoutParams(vat_layout_params);
		vt = (TextView)findViewById(R.id.vat);
		LinearLayout.LayoutParams vt_params = new LinearLayout.LayoutParams(
				(int)(110*rx),LinearLayout.LayoutParams.FILL_PARENT);
		vt_params.gravity = Gravity.LEFT;
		vt.setLayoutParams(vt_params);
		vt.setTextSize((14f*ry)/global.f);
		vt.setPadding((int)(5*rx),0,0,0);
		vtid = (TextView)findViewById(R.id.vatid);
		LinearLayout.LayoutParams vtid_params = new LinearLayout.LayoutParams(
				(int)(200*rx),LinearLayout.LayoutParams.FILL_PARENT);
		vtid_params.gravity = Gravity.RIGHT;
		vtid.setLayoutParams(vtid_params);
		vtid.setTextSize((14f*ry)/global.f);
		vtid.setPadding(0,0,(int)(5*rx),0);

		LinearLayout chrg_layout = (LinearLayout)findViewById(R.id.charge_layout);
		LinearLayout.LayoutParams chrg_layout_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		chrg_layout_params.gravity = Gravity.RIGHT;
		chrg_layout_params.setMargins(0, 0, (int)(5*rx), (int)(5*ry));
		chrg_layout.setLayoutParams(chrg_layout_params);
		st = (TextView)findViewById(R.id.charge);
		LinearLayout.LayoutParams st_params = new LinearLayout.LayoutParams(
				(int)(175*rx),LinearLayout.LayoutParams.FILL_PARENT);
		st_params.gravity = Gravity.LEFT;
		st.setLayoutParams(st_params);
		st.setTextSize((14f*ry)/global.f);
		st.setPadding((int)(5*rx),0,0,0);
		chid= (TextView)findViewById(R.id.chargeID);
		LinearLayout.LayoutParams chid_params = new LinearLayout.LayoutParams(
				(int)(135*rx),LinearLayout.LayoutParams.FILL_PARENT);
		chid_params.gravity = Gravity.RIGHT;
		chid.setLayoutParams(chid_params);
		chid.setTextSize((14f*ry)/global.f);
		chid.setPadding(0,0,(int)(5*rx),0);

		LinearLayout total_layout = (LinearLayout)findViewById(R.id.total_layout);
		LinearLayout.LayoutParams total_layout_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		total_layout_params.gravity = Gravity.RIGHT;
		total_layout_params.setMargins(0, 0, (int)(5*rx), (int)(5*ry));
		total_layout.setLayoutParams(total_layout_params);
		gt = (TextView)findViewById(R.id.gtotal);
		LinearLayout.LayoutParams total_params = new LinearLayout.LayoutParams(
				(int)(110*rx),LinearLayout.LayoutParams.FILL_PARENT);
		total_params.gravity = Gravity.LEFT;
		gt.setLayoutParams(total_params);
		gt.setTextSize((14f*ry)/global.f);
		gt.setPadding((int)(5*rx),0,0,0);
		gid = (TextView)findViewById(R.id.gtotalID);
		LinearLayout.LayoutParams gid_params = new LinearLayout.LayoutParams(
				(int)(200*rx),LinearLayout.LayoutParams.FILL_PARENT);
		gid_params.gravity = Gravity.RIGHT;
		gid.setLayoutParams(gid_params);
		gid.setTextSize((14f*ry)/global.f);
		gid.setPadding(0,0,(int)(5*rx),0);

		//LinearLayout tc_layout = (LinearLayout)findViewById(R.id.tc_layout);
		LinearLayout.LayoutParams tc_layout_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		tc_layout_params.gravity = Gravity.RIGHT;
		tc_layout_params.setMargins(0, 0, (int)(5*rx), (int)(5*ry));
		//tc_layout.setLayoutParams(tc_layout_params);
		//tc = (TextView)findViewById(R.id.tobacco_str);
		LinearLayout.LayoutParams tc_params = new LinearLayout.LayoutParams(
				(int)(200*rx),LinearLayout.LayoutParams.FILL_PARENT);
		tc_params.gravity = Gravity.LEFT;
		//tc.setLayoutParams(tc_params);
		//tc.setTextSize((14f*ry)/global.f);
		//tc.setPadding((int)(5*rx),0,0,0);
		//tc_txt = (TextView)findViewById(R.id.tobacco);
		LinearLayout.LayoutParams tctxt_params = new LinearLayout.LayoutParams(
				(int)(110*rx),LinearLayout.LayoutParams.FILL_PARENT);
		tctxt_params.gravity = Gravity.RIGHT;
		//tc_txt.setLayoutParams(tctxt_params);
		//tc_txt.setTextSize((14f*ry)/global.f);
		//tc_txt.setPadding(0,0,(int)(5*rx),0);

		LinearLayout gtotal_layout = (LinearLayout)findViewById(R.id.gtotal_layout);
		LinearLayout.LayoutParams gtotal_layout_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		gtotal_layout_params.gravity = Gravity.RIGHT;
		gtotal_layout_params.setMargins(0, 0, (int)(5*rx), (int)(5*ry));
		gtotal_layout.setLayoutParams(gtotal_layout_params);
		totalView = (TextView)findViewById(R.id.total);
		LinearLayout.LayoutParams gtotal_params = new LinearLayout.LayoutParams(
				(int)(200*rx),LinearLayout.LayoutParams.FILL_PARENT);
		gtotal_params.gravity = Gravity.LEFT;
		totalView.setLayoutParams(gtotal_params);
		totalView.setTextSize((14f*ry)/global.f);
		totalView.setPadding((int)(5*rx),0,0,0);
		totalTK = (TextView)findViewById(R.id.totalID);
		LinearLayout.LayoutParams ttk_params = new LinearLayout.LayoutParams(
				(int)(110*rx),LinearLayout.LayoutParams.FILL_PARENT);
		ttk_params.gravity = Gravity.RIGHT;
		totalTK.setLayoutParams(ttk_params);
		totalTK.setTextSize((14f*ry)/global.f);
		totalTK.setPadding(0,0,(int)(5*rx),0);

		LinearLayout button_layout = (LinearLayout)findViewById(R.id.button_layout);
		LinearLayout.LayoutParams button_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		button_params.gravity = Gravity.RIGHT;
		button_params.setMargins(0, 0, (int)(5*rx),0);
		button_layout.setLayoutParams(button_params);
		cancel_button = (Button)findViewById(R.id.cancel_button);
		cancel_button.setOnClickListener(cancel_button_listener);
		LinearLayout.LayoutParams cancel_params = new LinearLayout.LayoutParams(
				(int)(100*rx),(int)(32*ry));
		cancel_button.setLayoutParams(cancel_params);
		cancel_button.setTextSize((12f*ry)/global.f);
		clear_button = (Button)findViewById(R.id.clear_button);
		clear_button.setOnClickListener(clear_button_listener);
		LinearLayout.LayoutParams clear_params = new LinearLayout.LayoutParams(
				(int)(100*rx),(int)(32*ry));
		clear_params.setMargins((int)(5*rx),0,0,0);
		clear_button.setLayoutParams(clear_params);
		clear_button.setTextSize((12f*ry)/global.f);
		post_button = (Button)findViewById(R.id.post_button);
		post_button.setOnClickListener(post_button_listener);
		LinearLayout.LayoutParams post_params = new LinearLayout.LayoutParams(
				(int)(100*rx),(int)(32*ry));
		post_params.setMargins((int)(5*rx),0,0,0);
		post_button.setLayoutParams(post_params);
		post_button.setTextSize((12f*ry)/global.f);	
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.confirmation_window);

		df = new DecimalFormat("###.##");

		box = new ArrayList<CheckBox>();
		amount_view = new ArrayList<TextView>();
		plus_layout = new ArrayList<LinearLayout>();
		min_layout = new ArrayList<LinearLayout>();
		total_view = new ArrayList<TextView>();
		tc_box = new ArrayList<CheckBox>();
		tc_amount_view = new ArrayList<TextView>();
		tc_plus_layout = new ArrayList<LinearLayout>();
		tc_min_layout = new ArrayList<LinearLayout>();
		tc_total_view = new ArrayList<TextView>();



		dialog = new AlertDialog.Builder(this);
		alertDialog = new AlertDialog.Builder(this).create();

		global = new Global();
		rx = global.getRx();
		ry = global.getRy();
		setElements();
		global.setDatahelper(new DataHelper(this));
		dh = global.getDatahelper();
		/*
		 * uid-_-food id-_-food name-_-cat id-_-amount-_-description-_-
		 * food price-_-total price-_-timestamp####uid2-_-food id2-_-
		 * food name2-_-cat id2-_-amount2-_-description2-_-food price2-_-
		 * total price2-_-timestamp2
		 */
		//sb = new StringBuilder();
		table = dh.selectAll();
		totaltotal = 0.0;
		Conftable = (TableLayout)findViewById(R.id.ConfirmationTable);
		//		tc_Conftable = (TableLayout)findViewById(R.id.TobaccoTable);
		if(table.size()>0){
			for(String x : table){
				//  Log.d("TAG",x);
				temp = x.split("-_-");
				TableRow ctr = new TableRow(this);
				//Log.d("TAG","TableRowId " + ctr.getId());
				ctr.setLayoutParams(new TableRow.LayoutParams(
						LayoutParams.FILL_PARENT,
						LayoutParams.WRAP_CONTENT));
				//				ctr.setBackgroundColor(android.graphics.Color.BLACK);
				ctr.addView(conf_view_gen(temp[2], temp[5], temp[6], temp[4], temp[7]));                
				totaltotal+=Double.parseDouble(temp[7]);
				Conftable.addView(ctr,new TableLayout.LayoutParams(
						LayoutParams.FILL_PARENT,
						LayoutParams.WRAP_CONTENT));

				//sb.append(uid+"-_-"+x+"-_-"+timestamp+"####");
			}
		}
		buildTobacco();
		if(tc_list.size()>0){
			for(int i=0;i<tc_list.size();i++){
				TableRow tc_row = new TableRow(this);
				tc_row.setLayoutParams(new TableRow.LayoutParams(
						LayoutParams.FILL_PARENT,
						LayoutParams.WRAP_CONTENT));
				tc_row.addView(tc_view_gen(
						tc_list.get(i).food_name, 
						tc_list.get(i).desc, 
						tc_list.get(i).FoodPrice+"", 
						tc_list.get(i).amount+"", 
						tc_list.get(i).TotalPrice+""));
				/*tc_Conftable.addView(tc_row,new TableLayout.LayoutParams(
						LayoutParams.FILL_PARENT,
						LayoutParams.WRAP_CONTENT));*/
			}
		}else
			tobacco_is_empty();






		calculate_tobacco_price();
		calculate_total_price();

		vt.setText("Vat: "+ global.getVat()+"%");
		st.setText("Service Charge: " + global.getService()+"%");
		gt.setText("Total: ");
		//POST_String = sb.toString();
	}
	public LinearLayout tc_view_gen(String food_name, String ing, String unit_price, String amount, String total){
		//container layout
		LinearLayout cont = new LinearLayout(this);
		cont.setLayoutParams(new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));

		//food_layout
		LinearLayout food_layout = new LinearLayout(this);
		tc_box.add(new CheckBox(this));
		tc_box.get(tc_box.size()-1).setLayoutParams(new LayoutParams(
				LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
		//		tc_box.get(tc_box.size()-1).setOnCheckedChangeListener(tc_uncheck_action_listener);//will be tc_uncheck_action_listener
		tc_box.get(tc_box.size()-1).setChecked(true);
		tc_box.get(tc_box.size()-1).setText(food_name+" "+ing);
		tc_box.get(tc_box.size()-1).setTextSize((18f*ry)/global.f);
		tc_box.get(tc_box.size()-1).setTextColor(android.graphics.Color.WHITE);
		tc_box.get(tc_box.size()-1).setButtonDrawable(R.drawable.checkbox);
		//		box.get(box.size()-1).setBackgroundColor(android.graphics.Color.BLUE);
		food_layout.setLayoutParams(new LayoutParams(
				(int)(200*rx),LayoutParams.WRAP_CONTENT));
		food_layout.setPadding((int)(10*rx), 0, 0, 0);
		food_layout.addView(tc_box.get(tc_box.size()-1));

		cont.addView(food_layout);

		//		LinearLayout up_layout = new LinearLayout(this);
		//		up_layout.setLayoutParams(new LayoutParams(155,LayoutParams.WRAP_CONTENT));
		//		TextView up_view = new TextView(this);
		//		up_view.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
		//		up_view.setText(unit_price);
		//		up_view.setGravity(Gravity.CENTER);
		//		up_view.setBackgroundColor(android.graphics.Color.RED);
		//		up_view.setTextColor(android.graphics.Color.WHITE);
		//		up_view.setTypeface(null,android.graphics.Typeface.BOLD);
		//		up_view.setTextSize(16f);
		//		up_layout.setPadding(7, 0, 0, 0);
		//		up_layout.addView(up_view);
		//		cont.addView(up_layout);

		//amount_layout
		LinearLayout amount_layout = new LinearLayout(this);
		amount_layout.setLayoutParams(new LayoutParams((int)(105*rx),(int)(30*ry)));
		amount_layout.setPadding((int)(7*rx), 0, 0, 0);
		//		amount_layout.setBackgroundColor(android.graphics.Color.YELLOW);
		tc_plus_layout.add(new LinearLayout(this));

		tc_plus_layout.get(tc_box.size()-1).setLayoutParams(new LayoutParams(
				(int)(26*rx),(int)(28*ry)));
		tc_plus_layout.get(tc_box.size()-1).setId(tc_box.size()-1);
		tc_plus_layout.get(tc_box.size()-1).setOnClickListener(tc_plus_amount_listener);//tc_plus_amount_listener
		tc_plus_layout.get(tc_box.size()-1).setBackgroundResource(R.drawable.plus_button);
		//		TextView plusText = new TextView(this);
		//		plusText.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
		//		plusText.setGravity(Gravity.CENTER);
		//		plusText.setTextColor(android.graphics.Color.WHITE);
		//		plusText.setTypeface(null,Typeface.BOLD);
		//		plus_layout.get(box.size()-1).addView(plusText);
		amount_layout.addView(tc_plus_layout.get(tc_box.size()-1));

		tc_amount_view.add(new TextView(this));
		tc_amount_view.get(tc_box.size()-1).setLayoutParams(new LayoutParams(
				(int)(45*rx),LayoutParams.WRAP_CONTENT));
		tc_amount_view.get(tc_box.size()-1).setText(amount);
		tc_amount_view.get(tc_box.size()-1).setTextColor(android.graphics.Color.WHITE);
		tc_amount_view.get(tc_box.size()-1).setTypeface(null,android.graphics.Typeface.BOLD);
		tc_amount_view.get(tc_box.size()-1).setTextSize((16f*ry)/global.f);
		tc_amount_view.get(tc_box.size()-1).setGravity(Gravity.CENTER);
		amount_layout.addView(tc_amount_view.get(tc_box.size()-1));

		tc_min_layout.add(new LinearLayout(this));
		tc_min_layout.get(tc_box.size()-1).setLayoutParams(new LayoutParams(
				(int)(26*rx),(int)(28*ry)));
		tc_min_layout.get(tc_box.size()-1).setId(tc_box.size()-1);
		tc_min_layout.get(tc_box.size()-1).setOnClickListener(tc_min_amount_listener);//tc_min_amount_listener
		tc_min_layout.get(tc_box.size()-1).setBackgroundResource(R.drawable.minus_button);
		//		TextView minText = new TextView(this);
		//		minText.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
		//		minText.setText("  -");
		//		minText.setGravity(Gravity.CENTER);
		//		minText.setTextColor(android.graphics.Color.WHITE);
		//		minText.setTypeface(null,Typeface.BOLD);
		//		min_layout.get(box.size()-1).addView(minText);
		amount_layout.addView(tc_min_layout.get(tc_box.size()-1));

		cont.addView(amount_layout);

		//total_layout
		LinearLayout total_layout = new LinearLayout(this);
		total_layout.setLayoutParams(new LayoutParams((int)(115*rx),LayoutParams.WRAP_CONTENT));
		total_layout.setPadding((int)(5*rx), 0, 0, 0);
		tc_total_view.add(new TextView(this));
		tc_total_view.get(tc_box.size()-1).setLayoutParams(new LayoutParams(
				LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
		tc_total_view.get(tc_box.size()-1).setText(total);
		tc_total_view.get(tc_box.size()-1).setGravity(Gravity.CENTER);
		//		total_view.get(box.size()-1).setPadding(5, 0, 0, 0);
		tc_total_view.get(tc_box.size()-1).setTextColor(android.graphics.Color.WHITE);
		tc_total_view.get(tc_box.size()-1).setTypeface(null,android.graphics.Typeface.BOLD);
		tc_total_view.get(tc_box.size()-1).setTextSize((16f*ry)/global.f);
		//		total_view.get(box.size()-1).setBackgroundColor(android.graphics.Color.CYAN);
		total_layout.addView(tc_total_view.get(tc_box.size()-1));

		cont.addView(total_layout);

		return cont;
	}
	TextView vt, st, gt;
	public LinearLayout conf_view_gen(String food_name, String ing, String unit_price, String amount, String total){

		//container layout
		LinearLayout cont = new LinearLayout(this);
		cont.setLayoutParams(new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));

		//food_layout
		LinearLayout food_layout = new LinearLayout(this);
		box.add(new CheckBox(this));
		box.get(box.size()-1).setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		box.get(box.size()-1).setOnCheckedChangeListener(uncheck_action_listener);
		box.get(box.size()-1).setChecked(true);
		box.get(box.size()-1).setText(food_name+" "+ing);
		box.get(box.size()-1).setTextColor(android.graphics.Color.WHITE);
		box.get(box.size()-1).setTextSize((18f*ry)/global.f);
		box.get(box.size()-1).setButtonDrawable(R.drawable.checkbox);
		//		box.get(box.size()-1).setBackgroundColor(android.graphics.Color.BLUE);
		food_layout.setLayoutParams(new LayoutParams((int)(415*rx),LayoutParams.WRAP_CONTENT));
		food_layout.setPadding((int)(10*rx), 0, 0, 0);
		food_layout.addView(box.get(box.size()-1));
		cont.addView(food_layout);
		//ingredients_layout
		//		LinearLayout ing_layout = new LinearLayout(this);
		//		ing_layout.setLayoutParams(new LayoutParams(157,LayoutParams.WRAP_CONTENT));
		//		TextView ing_view = new TextView(this);
		//		ing_view.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
		//		ing_view.setText(ing);
		//		ing_view.setGravity(Gravity.LEFT);
		//		ing_layout.setPadding(7, 0, 0, 0);
		//		ing_layout.addView(ing_view);
		//
		//		cont.addView(ing_layout);
		//unit_price_layout
		LinearLayout up_layout = new LinearLayout(this);
		up_layout.setLayoutParams(new LayoutParams((int)(155*rx),LayoutParams.WRAP_CONTENT));
		TextView up_view = new TextView(this);
		up_view.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
		up_view.setText(unit_price);
		up_view.setGravity(Gravity.CENTER);
		//		up_view.setBackgroundColor(android.graphics.Color.RED);
		up_view.setTextColor(android.graphics.Color.WHITE);
		up_view.setTypeface(null,android.graphics.Typeface.BOLD);
		up_view.setTextSize((16f*ry)/global.f);
		up_layout.setPadding((int)(7*rx), 0, 0, 0);
		up_layout.addView(up_view);
		cont.addView(up_layout);

		//amount_layout
		LinearLayout amount_layout = new LinearLayout(this);
		amount_layout.setLayoutParams(new LayoutParams((int)(105*rx),(int)(30*ry)));
		amount_layout.setPadding((int)(5*rx), 0, 0, 0);
		//		amount_layout.setBackgroundColor(android.graphics.Color.YELLOW);
		plus_layout.add(new LinearLayout(this));

		plus_layout.get(box.size()-1).setLayoutParams(new LayoutParams(
				(int)(26*rx),(int)(28*ry)));
		plus_layout.get(box.size()-1).setId(box.size()-1);
		plus_layout.get(box.size()-1).setOnClickListener(plus_amount_listener);
		plus_layout.get(box.size()-1).setBackgroundResource(R.drawable.plus_button);
		//		TextView plusText = new TextView(this);
		//		plusText.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
		//		plusText.setGravity(Gravity.CENTER);
		//		plusText.setTextColor(android.graphics.Color.WHITE);
		//		plusText.setTypeface(null,Typeface.BOLD);
		//		plus_layout.get(box.size()-1).addView(plusText);
		amount_layout.addView(plus_layout.get(box.size()-1));

		amount_view.add(new TextView(this));
		amount_view.get(box.size()-1).setLayoutParams(new LayoutParams(
				(int)(45*rx),LayoutParams.WRAP_CONTENT));
		amount_view.get(box.size()-1).setText(amount);
		amount_view.get(box.size()-1).setTextColor(android.graphics.Color.WHITE);
		amount_view.get(box.size()-1).setTypeface(null,android.graphics.Typeface.BOLD);
		amount_view.get(box.size()-1).setTextSize((16f*ry)/global.f);
		amount_view.get(box.size()-1).setGravity(Gravity.CENTER);
		amount_layout.addView(amount_view.get(box.size()-1));

		min_layout.add(new LinearLayout(this));
		min_layout.get(box.size()-1).setLayoutParams(new LayoutParams(
				(int)(26*rx),(int)(28*ry)));
		min_layout.get(box.size()-1).setId(box.size()-1);
		min_layout.get(box.size()-1).setOnClickListener(min_amount_listener);
		min_layout.get(box.size()-1).setBackgroundResource(R.drawable.minus_button);
		//		TextView minText = new TextView(this);
		//		minText.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
		//		minText.setText("  -");
		//		minText.setGravity(Gravity.CENTER);
		//		minText.setTextColor(android.graphics.Color.WHITE);
		//		minText.setTypeface(null,Typeface.BOLD);
		//		min_layout.get(box.size()-1).addView(minText);
		amount_layout.addView(min_layout.get(box.size()-1));

		cont.addView(amount_layout);

		//total_layout
		LinearLayout total_layout = new LinearLayout(this);
		total_layout.setLayoutParams(new LayoutParams((int)(115*rx),
				LayoutParams.WRAP_CONTENT));
		total_layout.setPadding((int)(5*rx), 0, 0, 0);
		total_view.add(new TextView(this));
		total_view.get(box.size()-1).setLayoutParams(new LayoutParams(
				LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
		total_view.get(box.size()-1).setText(total);
		total_view.get(box.size()-1).setGravity(Gravity.CENTER);
		//		total_view.get(box.size()-1).setPadding(5, 0, 0, 0);
		total_view.get(box.size()-1).setTextColor(android.graphics.Color.WHITE);
		total_view.get(box.size()-1).setTypeface(null,android.graphics.Typeface.BOLD);
		total_view.get(box.size()-1).setTextSize((16f*ry)/global.f);
		//		total_view.get(box.size()-1).setBackgroundColor(android.graphics.Color.CYAN);
		total_layout.addView(total_view.get(box.size()-1));

		cont.addView(total_layout);

		return cont;
	}
	private void tobacco_is_empty(){
		LinearLayout tll = new LinearLayout(ConfWin.this);
		tll.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));
		//		tll.setBackgroundColor(android.graphics.Color.WHITE);
		TextView fmtv = new TextView(ConfWin.this);
		fmtv.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));
		fmtv.setText("No Tobacco Item is Listed");
		fmtv.setTextColor(android.graphics.Color.WHITE);
		fmtv.setTypeface(null,android.graphics.Typeface.BOLD);
		fmtv.setTextSize((25f*ry)/global.f);
		fmtv.setGravity(Gravity.CENTER);

		tll.addView(fmtv);
		TableRow tc_row = new TableRow(this);
		tc_row.setLayoutParams(new TableRow.LayoutParams(
				LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));
		tc_row.addView(tll);
		/*tc_Conftable.addView(tc_row,new TableLayout.LayoutParams(
				LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));*/
	}
	/*	private OnCheckedChangeListener tc_uncheck_action_listener = new OnCheckedChangeListener() {

		public void onCheckedChanged(CompoundButton arg0, boolean checked) {
			if(!checked){
				dialog
				.setTitle("Item Remove!")  
				.setMessage("Are you Sure you want to remove this item?")  
				.setPositiveButton("YES", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						//do stuff onclick of YES
						//***********
						//remove the corresponding rowId()
						int rowId = getUncheckedTobaccoId();

						if(rowId!=-1){

							tc_list.remove(rowId);
							updateTobacco();
							buildTobacco();
							//						Toast.makeText(ConfWin.this, "row id: "+rowId, 5).show();
						}
						//						updateTable();
						calculate_tobacco_price();
						calculate_total_price();
						if(tc_list.size() == 0){
							tobacco_is_empty();
						}
						if(table.isEmpty()&& tc_list.size()==0)
							finish();
						//************
						//                        startActivity(new Intent("ListCategoryView"));
						//finish();
						return;
					}
				})
				.setNegativeButton("NO", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						//do stuff onclick of NO
						setChecked();
						return;
						//finish();
					}
				}).show(); 
			}
		}
	};*/
	private OnCheckedChangeListener uncheck_action_listener = new OnCheckedChangeListener() {

		public void onCheckedChanged(CompoundButton arg0, boolean checked) {
			if(!checked){
				dialog
				.setTitle("Item Remove!")  
				.setMessage("Are you Sure you want to remove this item?")  
				.setPositiveButton("YES", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						//do stuff onclick of YES
						//***********
						//remove the corresponding rowId()
						long rowId = getUncheckedRowId();

						if(rowId!=-1)
							dh.deleteRow(rowId);
						//						updateTable();
						calculate_total_price();
						if(table.isEmpty()&& global.getTobaccoList().size()==0)
							finish();
						//************
						//                        startActivity(new Intent("ListCategoryView"));
						//finish();
						return;
					}
				})
				.setNegativeButton("NO", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						//do stuff onclick of NO
						setChecked();
						return;
						//finish();
					}
				}).show(); 
			}
		}
	};

	ArrayList<Tobacco>tc_list = new ArrayList<Tobacco>();
	public void updateTobacco(){
		global.setTobaccoList(tc_list);
	}
	public void buildTobacco(){
		tc_list = global.getTobaccoList();
		for(int i=0;i<tc_list.size();i++){

			tc_list.get(i).rowId = i;
		}
	}
	public void updateTable(){
		table = dh.selectAll();
	}
	/*public int getUncheckedTobaccoId(){
		for(int i=0;i<tc_box.size();i++){
			if(!tc_box.get(i).isChecked()){
				tc_box.remove(i);
				tc_amount_view.remove(i);
				tc_plus_layout.remove(i);
				tc_min_layout.remove(i);
				tc_total_view.remove(i);
				//tc_Conftable.removeViewAt(i);
				for(int j=i;j<tc_plus_layout.size();j++){
					tc_plus_layout.get(j).setId((tc_plus_layout.get(j).getId())-1);
					tc_min_layout.get(j).setId((tc_min_layout.get(j).getId())-1);

				}
				return tc_list.get(i).rowId;

			}
		}
		return -1;
	}*/
	public long getUncheckedRowId(){
		for(int i=0;i<box.size();i++){
			if(!box.get(i).isChecked()){
				box.remove(i);
				amount_view.remove(i);
				plus_layout.remove(i);
				min_layout.remove(i);
				total_view.remove(i);

				global.getItem_amount().remove(i);
				global.getItem_name().remove(i);
				global.getItem_price().remove(i);
				for(int j=i;j<plus_layout.size();j++){
					plus_layout.get(j).setId((plus_layout.get(j).getId())-1);
					min_layout.get(j).setId((min_layout.get(j).getId())-1);

				}
				updateTable();
				String x = table.get(i);
				temp = x.split("-_-");

				String b = "\\|"+temp[2]+" "+temp[6]+"\\|\\\n";
				global.updateBckt((global.getBckt()).replaceFirst(b, ""));
				Conftable.removeViewAt(i);
				global.setCount(global.getCount()-1);
				return Long.parseLong(temp[0]);
			}
		}
		return -1;
	}

	public void setChecked(){
		for(int i=0;i<box.size();i++){
			if(!box.get(i).isChecked()){
				box.get(i).setChecked(true);
			}
		}
		for(int j=0;j<tc_box.size();j++){
			if(!tc_box.get(j).isChecked()){
				tc_box.get(j).setChecked(true);
			}
		}
	}

	public OnClickListener plus_amount_listener = new OnClickListener() {

		public void onClick(View v) {
			// Log.d("TAG","PLUS ID IS:"+v.getId());
			double ttl = Double.parseDouble((total_view.get(v.getId()).getText()).toString());
			int amnt = (Integer.parseInt(((amount_view.get(v.getId())).getText()).toString()));
			ttl/=amnt;
			ttl*=(amnt+1);
			total_view.get(v.getId()).setText(""+ttl);
			amount_view.get(v.getId()).setText(""+(amnt+1));
			String x = table.get(v.getId());
			temp = x.split("-_-");
			temp[4] = (amnt+1)+"";
			temp[7] = ""+ttl;
			dh.updateRow(Long.parseLong(temp[0]),
					temp[1],temp[2],temp[3],temp[4],temp[5],"",temp[6],temp[7]);
			global.getItem_amount().set(v.getId(),Integer.parseInt(temp[4]));
			global.getItem_price().set(v.getId(),Double.parseDouble(temp[7]));
			calculate_total_price();
		}
	}; 
	public OnClickListener min_amount_listener = new OnClickListener(){
		public void onClick(View v) {
			Log.d("TAG","MINUS ID IS:"+v.getId());
			if((Integer.parseInt(((amount_view.get(v.getId())).getText()).toString()))==1)
			{
				Toast.makeText(ConfWin.this, "Cannot Choose less then 1 item, you \ncan remove the item by unchecking it", 5).show();
			}
			else{
				double ttl = Double.parseDouble((total_view.get(v.getId()).getText()).toString());
				int amnt = (Integer.parseInt(((amount_view.get(v.getId())).getText()).toString()));
				ttl/=amnt;
				ttl*=(amnt-1);
				total_view.get(v.getId()).setText(""+ttl);
				amount_view.get(v.getId()).setText(""+(amnt-1));
				String x = table.get(v.getId());
				temp = x.split("-_-");
				temp[4] = (amnt-1)+"";
				temp[7] = ""+ttl;
				dh.updateRow(Long.parseLong(temp[0]),
						temp[1],temp[2],temp[3],temp[4],temp[5],"",temp[6],temp[7]);
				global.getItem_amount().set(v.getId(),Integer.parseInt(temp[4]));
				global.getItem_price().set(v.getId(),Double.parseDouble(temp[7]));
				calculate_total_price();
			}
			//                amount_view.get(v.getId()).setText(""+((Integer.parseInt(((amount_view.get(v.getId())).getText()).toString()))-1));
		}
	};
	public OnClickListener tc_plus_amount_listener = new OnClickListener() {

		public void onClick(View v) {
			// Log.d("TAG","PLUS ID IS:"+v.getId());
			double ttl = Double.parseDouble((tc_total_view.get(v.getId()).getText()).toString());
			int amnt = (Integer.parseInt(((tc_amount_view.get(v.getId())).getText()).toString()));
			ttl/=amnt;
			ttl*=(amnt+1);
			tc_total_view.get(v.getId()).setText(""+ttl);
			tc_amount_view.get(v.getId()).setText(""+(amnt+1));
			//			String x = table.get(v.getId());
			//			temp = x.split("-_-");
			//			temp[4] = ""+(amnt+1);
			//			temp[7] = ""+ttl;
			//			dh.updateRow(Long.parseLong(temp[0]),
			//					temp[1],temp[2],temp[3],temp[4],temp[5],"",temp[6],temp[7]);
			//			global.getItem_amount().set(v.getId(),Integer.parseInt(temp[4]));
			//			global.getItem_price().set(v.getId(),Double.parseDouble(temp[7]));
			tc_list.get(v.getId()).amount = amnt+1;
			tc_list.get(v.getId()).TotalPrice = ttl;
			updateTobacco();
			buildTobacco();
			calculate_tobacco_price();
			calculate_total_price();
		}
	}; 
	public OnClickListener tc_min_amount_listener = new OnClickListener(){
		public void onClick(View v) {
			Log.d("TAG","MINUS ID IS:"+v.getId());
			if((Integer.parseInt(((tc_amount_view.get(v.getId())).getText()).toString()))==1)
			{
				Toast.makeText(ConfWin.this, "Cannot Choose less then 1 item, you \ncan remove the item by unchecking it", 5).show();
			}
			else{
				double ttl = Double.parseDouble((tc_total_view.get(v.getId()).getText()).toString());
				int amnt = (Integer.parseInt(((tc_amount_view.get(v.getId())).getText()).toString()));
				ttl/=amnt;
				ttl*=(amnt-1);
				tc_total_view.get(v.getId()).setText(""+ttl);
				tc_amount_view.get(v.getId()).setText(""+(amnt-1));
				//				String x = table.get(v.getId());
				//				temp = x.split("-_-");
				//				temp[4] = ""+(amnt-1);
				//				temp[7] = ""+ttl;
				//				dh.updateRow(Long.parseLong(temp[0]),
				//						temp[1],temp[2],temp[3],temp[4],temp[5],"",temp[6],temp[7]);
				//				global.getItem_amount().set(v.getId(),Integer.parseInt(temp[4]));
				//				global.getItem_price().set(v.getId(),Double.parseDouble(temp[7]));
				tc_list.get(v.getId()).amount = amnt-1;
				tc_list.get(v.getId()).TotalPrice = ttl;
				updateTobacco();
				buildTobacco();
				calculate_tobacco_price();
				calculate_total_price();
			}
			//                amount_view.get(v.getId()).setText(""+((Integer.parseInt(((amount_view.get(v.getId())).getText()).toString()))-1));
		}
	};
	public void calculate_tobacco_price(){
		tobacco_price = 0.0;
		updateTobacco();
		if(tc_list.size()>0){
			for(int i=0;i<tc_list.size();i++){
				tobacco_price+=tc_list.get(i).TotalPrice;
			}
		}
		//tc_txt.setText("TK: "+tobacco_price);
	}
	double vat, charge, gtotal,tobacco_price;
	public void calculate_total_price(){
		totaltotal = 0.0;
		updateTable();
		for(String s:table){
			temp = s.split("-_-");
			//			if(temp[2].equals(global.getIsTobacco())){
			//				tobacco_price = Double.parseDouble(temp[7]);
			//			}else
			totaltotal+=Double.parseDouble(temp[7]);
		}
		//		TextView tobaccoView = (TextView)findViewById(R.id.tobacco);
		//		if(!(global.getIsTobacco().equals("")))
		//			tobaccoView.setText("Tobacco: TK: "+df.format(tobacco_price));
		//		else
		//			tobaccoView.setText("Tobacco: TK: "+"0.0");
		vat = (totaltotal*global.getVat())/100;
		charge = (totaltotal*global.getService())/100;
		gtotal = totaltotal+vat+charge+tobacco_price;
		vt.setText("Vat: "+ global.getVat()+"% ");

		vtid.setText("TK: "+df.format(vat));

		st.setText("Service Charge: " + global.getService()+"% ");
		chid.setText("TK: "+df.format(charge));

		gid.setText("TK: "+df.format(totaltotal));
		//		gt.setText("SUBTOTAL: ");

		totalView.setText("Grand Total:(incl.vat & tax)");

		totalTK.setText("TK: "+df.format(gtotal));
	}

	private OnClickListener clear_button_listener = new OnClickListener(){

		public void onClick(View arg0) {

			dialog
			.setTitle("Oops!")  
			.setMessage("Clear everything?")  
			.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					//do stuff onclick of YES

					dh.deleteAll();
					global.clearBckt();
					global.setCount(0);
					global.getTobaccoList().clear();
					startActivity(new Intent("ListCategoryView").addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
					finish();
				}
			})
			.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					//do stuff onclick of YES
					return;
					//finish();
				}
			}).show();
			//alertDialog.show();
		}
	};

	private OnClickListener cancel_button_listener = new OnClickListener() {

		public void onClick(View v) {
			global.updateBckt(global.getBckt());
			finish();
		}
	};
	String timestamp;
	RestClient client;
	String tobacco_tag = "{tobacco}";
	private OnClickListener post_button_listener = new OnClickListener() {

		public void onClick(View v) {
			mHour = new Random().nextInt(24);
			mMinute = new Random().nextInt(60);
			timestamp = mHour+":"+mMinute;
			//  String uid = ""+new Random().nextInt(100);
			if(global.isOnline){
				try{
					//List<String>table = dh.selectAll();

					dialog
					.setTitle("Order Confirmation")  
					.setMessage("Confirm Order?")  
					.setPositiveButton("YES", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {

							try{
								updateTable();
								StringBuilder sb = new StringBuilder();
								for(String x:table){
									sb.append(x+"-_-"+timestamp+"-_-"+global.getTABLE_ID()+
											"-_-"+global.getWAITER_ID()+"####");
									Log.d("LOG",x);
								}
								String tc_str = "";
								for(int i=0;i<tc_list.size();i++){

									tc_str+=tobacco_tag
											+tc_list.get(i).rowId+"-_-"
											+tc_list.get(i).id_food+"-_-"
											+tc_list.get(i).food_name+"-_-"
											+tc_list.get(i).id_cat+"-_-"
											+tc_list.get(i).amount+"-_-"
											+tc_list.get(i).desc+"-_-"
											+tc_list.get(i).FoodPrice+"-_-"
											+tc_list.get(i).TotalPrice+"-_-"
											+timestamp+"-_-"+global.getTABLE_ID()+"-_-"+global.getWAITER_ID()
											+tobacco_tag+"####"
											;
								}
								sb.append(tc_str);
								client = new RestClient(global.getOrder_tableURL());
								sb.append("$$$$"+gtotal);
								Log.i("Post Str",sb.toString());

								client.AddParam("long_str", sb.toString());
								client.Execute(RequestMethod.POST);
								//do stuff onclick of YES

								dh.deleteAll();
								global.clearBckt();
								global.setCount(0);
								global.getTobaccoList().clear();

								startActivity(new Intent("PostForm").addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
								finish();

								return;
							}catch(Exception e){}
						}
					})
					.setNegativeButton("NO", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
							//do stuff onclick of YES
							return;
							//finish();
						}
					}).show();
					//alertDialog.show();
					//Grand Total send
					/*
				alertDialog.setTitle("Congratulation!");  
				alertDialog.setMessage("Your order has been received");  
				alertDialog.setButton("OK", new DialogInterface.OnClickListener() {  
					public void onClick(DialogInterface dialog, int which) {  
						dh.deleteAll();
						global.clearBckt();
						global.setCount(0);

						//in order to skip the form
						//comment the below line
						startActivity(new Intent("PostForm"));
						finish();

						return;  
					} });   
				alertDialog.show();
					 */
					//	int respCode = client.getResponseCode();
					//				Log.d("TAG",client.getResponse());
				}catch(Exception e){
					Log.d("TAG",e.toString());
				}
			}
			else{Toast toast = Toast.makeText(ConfWin.this,"There is no Connection. Please try it online.", 5000); toast.show();}
		}
	};
	public void onClick(View v) {
		//Unused!!!
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(dh!=null)
			dh.close();
	}
}
