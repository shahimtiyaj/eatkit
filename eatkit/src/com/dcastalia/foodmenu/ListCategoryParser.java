package com.dcastalia.foodmenu;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ListCategoryParser extends DefaultHandler{

	ArrayList<ListCategory> list = new ArrayList<ListCategory>();
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		// TODO Auto-generated method stub
		super.startElement(uri, localName, qName, attributes);
		
		if(localName.equals("category")){
			list.add(new ListCategory(attributes.getValue("id"), attributes.getValue("name"),
					attributes.getValue("file"),attributes.getValue("image")));
		}
	}

}
