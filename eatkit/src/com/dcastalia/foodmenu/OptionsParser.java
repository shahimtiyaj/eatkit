package com.dcastalia.foodmenu;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

public class OptionsParser extends DefaultHandler {

    ArrayList<Table>tables = new ArrayList<Table>();
    ArrayList<Waiter>waiters = new ArrayList<Waiter>();
    ArrayList<Featured>flist = new ArrayList<Featured>();
    ArrayList<RestPhoto>rest_photo = new ArrayList<RestPhoto>();
    Restaurent restaurent;
    Commerce commerce;
    ArrayList<Ads>ads = new ArrayList<Ads>();
    StaticAd stad;
    int featuredId;
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        if(localName.equals("table")){
            tables.add(new Table(attributes.getValue("name")));
        }
        if(localName.equals("waiter")){
            waiters.add(new Waiter(attributes.getValue("name")));
        }
        if(localName.equals("item")){
            flist.add(new Featured(
                    attributes.getValue("id"), attributes.getValue("name"),
                    attributes.getValue("image"),attributes.getValue("desc"),
                    attributes.getValue("price"),attributes.getValue("unit")));
            
            featuredId  = Integer.parseInt(attributes.getValue("id"));
        }
        if(localName.equals("subitem")){
            flist.get(featuredId).setSubItem(
                    attributes.getValue("id"), attributes.getValue("name"),
                    attributes.getValue("price"),attributes.getValue("unit"));
        }
        if(localName.equals("restaurent")){
            restaurent = new Restaurent(attributes.getValue("logo"),
                    attributes.getValue("name"),attributes.getValue("desc"),
                    attributes.getValue("long_desc"),
                    attributes.getValue("open"),attributes.getValue("close"));
        }
        if(localName.equals("ritem")){
        	rest_photo.add(new RestPhoto(attributes.getValue("image")));
        }
        if(localName.equals("commerce")){
            commerce = new Commerce(attributes.getValue("vat"),attributes.getValue("service_charge"));
        }
        if(localName.equals("ad")){
            ads.add(new Ads(attributes.getValue("id"),
                    attributes.getValue("title"),attributes.getValue("description"),
                    attributes.getValue("link"),attributes.getValue("order"),
                    attributes.getValue("image")));
        }
        if(localName.equals("static")){
            stad = new StaticAd(attributes.getValue("id"),
                    attributes.getValue("title"), 
                    attributes.getValue("description"), 
                    attributes.getValue("link"), 
                    attributes.getValue("order"),
                    attributes.getValue("image"));
        }
    }
}
