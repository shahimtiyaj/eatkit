package com.dcastalia.foodmenu;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public class PostForm extends Activity implements OnClickListener{

	Global global;
	float rx,ry;

	//	private OptionsParser OP;
	//	private int nextImg = 0;
	private String user_name, user_email,user_phone, user_age,user_comment,
	user_occupation,user_holiday,user_isSmoker,about_menu,about_food,about_restaurant;
	EditText uname,uholiday,uemail,uphone,ucomment;
	Button uage,uskip,usend,uoccupation;
	CheckBox yes_checked,no_checked,menu_yes, menu_no,food_yes,food_no,restaurant_yes,restaurant_no;
	ScrollView qa_scroll;
	ImageView sc_down,sc_up;
	int scrollX, scrollY;
	Timer mmTimer;
	TableLayout qaTable;
	public void setElements(){
		ImageView ordereat_logo = (ImageView)findViewById(R.id.ordereatlogo);
		RelativeLayout.LayoutParams ordereat_params = new RelativeLayout.LayoutParams(
				(int)(100*rx),(int)(40*ry));
		ordereat_params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		ordereat_params.setMargins((int)(20*rx),0,0,0);
		ordereat_logo.setLayoutParams(ordereat_params);
		
		ImageView ferra_logo = (ImageView)findViewById(R.id.ferratechlogo);
		RelativeLayout.LayoutParams ferra_params = new RelativeLayout.LayoutParams(
				(int)(100*rx),(int)(40*ry));
		ferra_params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		ferra_params.setMargins(0,0,(int)(20*rx),0);
		ferra_logo.setLayoutParams(ferra_params);
		
		TextView pwr_txt = (TextView)findViewById(R.id.powered_text);
		RelativeLayout.LayoutParams pwr_params = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		pwr_params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		pwr_params.setMargins(0,(int)(20*ry),(int)(110*rx),0);
		pwr_txt.setLayoutParams(pwr_params);
		pwr_txt.setTextSize((12f*ry)/global.f);
		
		sc_down = (ImageView)findViewById(R.id.item_scroll_down);
		sc_down.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,(int)(12*ry)));
		sc_up = (ImageView)findViewById(R.id.item_scroll_up);
		sc_up.setLayoutParams(new LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,(int)(12*ry)));
		LinearLayout qa_layout = (LinearLayout)findViewById(R.id.scroller_id);
		qa_layout.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,(int)(225*ry)));
		qa_layout.setPadding((int)(15*rx),0,0,0);
		qa_scroll = (ScrollView)findViewById(R.id.qa_scroller);
		qa_scroll.setOnTouchListener(qa_sccroll_listener);
		TextView u_name = (TextView)findViewById(R.id.u_name);
		u_name.setLayoutParams(new LayoutParams(
				(int)(300*rx),(int)(40*ry)));
		u_name.setTextSize((16f*ry)/global.f);
		LinearLayout uname_layout = (LinearLayout)findViewById(R.id.u_name_layout);
		LayoutParams uname_params = new LayoutParams(
				(int)(400*rx),LayoutParams.FILL_PARENT);
		uname_params.setMargins(0, 0, 0, (int)(5*ry));
		uname_layout.setLayoutParams(uname_params);
		uname_layout.setPadding((int)(5*rx),(int)(5*ry),(int)(5*rx),(int)(5*ry));
		uname = (EditText)findViewById(R.id.u_name_text);
		uname.setTextSize((14f*ry)/global.f);
		TextView u_email = (TextView)findViewById(R.id.u_email);
		u_email.setLayoutParams(new LayoutParams(
				(int)(300*rx),(int)(40*ry)));
		u_email.setTextSize((16f*ry)/global.f);
		LinearLayout uemail_layout = (LinearLayout)findViewById(R.id.u_email_layout);
		LayoutParams uemail_params = new LayoutParams(
				(int)(400*rx),LayoutParams.FILL_PARENT);
		uemail_params.setMargins(0, 0, 0, (int)(5*ry));
		uemail_layout.setLayoutParams(uemail_params);
		uemail_layout.setPadding((int)(5*rx),(int)(5*ry),(int)(5*rx),(int)(5*ry));
		uemail = (EditText)findViewById(R.id.u_email_text);
		uemail.setTextSize((14f*ry)/global.f);
		TextView u_phone = (TextView)findViewById(R.id.u_phone);
		u_phone.setLayoutParams(new LayoutParams(
				(int)(300*rx),(int)(40*ry)));
		u_phone.setTextSize((16f*ry)/global.f);
		LinearLayout uphone_layout = (LinearLayout)findViewById(R.id.u_phone_layout);
		LayoutParams uphone_params = new LayoutParams(
				(int)(400*rx),LayoutParams.FILL_PARENT);
		uphone_params.setMargins(0, 0, 0, (int)(5*ry));
		uphone_layout.setLayoutParams(uphone_params);
		uphone_layout.setPadding((int)(5*rx),(int)(5*ry),(int)(5*rx),(int)(5*ry));
		uphone = (EditText)findViewById(R.id.u_number_text);
		uphone.setTextSize((14f*ry)/global.f);
		TextView u_holiday = (TextView)findViewById(R.id.u_holiday);
		u_holiday.setLayoutParams(new LayoutParams(
				(int)(300*rx),(int)(40*ry)));
		u_holiday.setTextSize((16f*ry)/global.f);
		LinearLayout uholiday_layout = (LinearLayout)findViewById(R.id.u_holiday_layout);
		LayoutParams uholiday_params = new LayoutParams(
				(int)(400*rx),LayoutParams.FILL_PARENT);
		uholiday_params.setMargins(0, 0, 0, (int)(5*ry));
		uholiday_layout.setLayoutParams(uholiday_params);
		uholiday_layout.setPadding((int)(5*rx),(int)(5*ry),(int)(5*rx),(int)(5*ry));
		uholiday = (EditText)findViewById(R.id.u_holiday_text);
		uholiday.setTextSize((14f*ry)/global.f);
		TextView u_occupation = (TextView)findViewById(R.id.u_occupation);
		u_occupation.setLayoutParams(new LayoutParams(
				(int)(300*rx),(int)(40*ry)));
		u_occupation.setTextSize((16f*ry)/global.f);
		RelativeLayout uoccupation_layout = (RelativeLayout)findViewById(R.id.occupation_layout);
		LayoutParams uoccupation_params = new LayoutParams(
				(int)(400*rx),LayoutParams.FILL_PARENT);
		uoccupation_params.setMargins(0, 0, 0, (int)(5*ry));
		uoccupation_layout.setLayoutParams(uoccupation_params);
		uoccupation_layout.setPadding((int)(5*rx),(int)(5*ry),(int)(5*rx),(int)(5*ry));
		uoccupation = (Button)findViewById(R.id.u_occupation_text);
		uoccupation.setOnClickListener(occupation_listener);
		RelativeLayout.LayoutParams occupation_button_params = new RelativeLayout.LayoutParams(
				(int)(200*rx),(int)(25*ry));
		occupation_button_params.addRule(RelativeLayout.CENTER_IN_PARENT);
		uoccupation.setLayoutParams(occupation_button_params);
		uoccupation.setGravity(Gravity.LEFT);
		uoccupation.setTextSize((14f*ry)/global.f);
		uoccupation.setPadding((int)(20*rx),0,0,0);
		TextView u_age = (TextView)findViewById(R.id.u_age);
		u_age.setLayoutParams(new LayoutParams(
				(int)(300*rx),(int)(40*ry)));
		u_age.setTextSize((16f*ry)/global.f);
		RelativeLayout uage_layout = (RelativeLayout)findViewById(R.id.age_layout);
		LayoutParams uage_params = new LayoutParams(
				(int)(400*rx),LayoutParams.FILL_PARENT);
		uage_params.setMargins(0, 0, 0, (int)(5*ry));
		uage_layout.setLayoutParams(uoccupation_params);
		uage_layout.setPadding((int)(5*rx),(int)(5*ry),(int)(5*rx),(int)(5*ry));
		uage = (Button)findViewById(R.id.u_age_button);
		uage.setOnClickListener(uage_listener);
		RelativeLayout.LayoutParams age_button_params = new RelativeLayout.LayoutParams(
				(int)(200*rx),(int)(25*ry));
		age_button_params.addRule(RelativeLayout.CENTER_IN_PARENT);
		uage.setLayoutParams(age_button_params);
		uage.setGravity(Gravity.LEFT);
		uage.setTextSize((14f*ry)/global.f);
		uage.setPadding((int)(20*rx),0,0,0);
		TextView u_smoker = (TextView)findViewById(R.id.u_smoker);
		u_smoker.setLayoutParams(new LayoutParams(
				(int)(300*rx),(int)(40*ry)));
		u_smoker.setTextSize((16f*ry)/global.f);		
		LinearLayout usmoke_layout = (LinearLayout)findViewById(R.id.smoke_layout);
		LayoutParams usmoke_params = new LayoutParams(
				(int)(400*rx),LayoutParams.FILL_PARENT);
		usmoke_params.setMargins(0, 0, 0, (int)(5*ry));
		usmoke_layout.setLayoutParams(usmoke_params);
		usmoke_layout.setPadding((int)(100*rx),(int)(0*ry),(int)(0*rx),(int)(0*ry));
		yes_checked = (CheckBox)findViewById(R.id.yes_checked);
		yes_checked.setOnCheckedChangeListener(yes_checked_listener);
		yes_checked.setLayoutParams(new LinearLayout.LayoutParams(
				(int)(120*rx),(int)(32*ry)));
		yes_checked.setTextSize((16f*ry)/global.f);
		no_checked = (CheckBox)findViewById(R.id.no_checked);
		no_checked.setOnCheckedChangeListener(no_checked_listener);
		no_checked.setLayoutParams(new LinearLayout.LayoutParams(
				(int)(120*rx),(int)(32*ry)));
		no_checked.setTextSize((16f*ry)/global.f);
		TextView u_menu = (TextView)findViewById(R.id.about_menu_text);
		u_menu.setLayoutParams(new LayoutParams(
				(int)(300*rx),(int)(40*ry)));
		u_menu.setTextSize((16f*ry)/global.f);		
		LinearLayout umenu_layout = (LinearLayout)findViewById(R.id.about_menu_layout);
		LayoutParams umenu_params = new LayoutParams(
				(int)(400*rx),LayoutParams.FILL_PARENT);
		umenu_params.setMargins(0, 0, 0, (int)(5*ry));
		umenu_layout.setLayoutParams(umenu_params);
		umenu_layout.setPadding((int)(100*rx),(int)(0*ry),(int)(0*rx),(int)(0*ry));
		menu_yes = (CheckBox)findViewById(R.id.menu_yes_checked);
		menu_yes.setOnCheckedChangeListener(menu_yes_listener);
		menu_yes.setLayoutParams(new LinearLayout.LayoutParams(
				(int)(120*rx),(int)(32*ry)));
		menu_yes.setTextSize((16f*ry)/global.f);
		menu_no = (CheckBox)findViewById(R.id.menu_no_checked);
		menu_no.setOnCheckedChangeListener(menu_no_listener);
		menu_no.setTextSize((16f*ry)/global.f);
		menu_no.setLayoutParams(new LinearLayout.LayoutParams(
				(int)(120*rx),(int)(32*ry)));
		TextView u_food = (TextView)findViewById(R.id.about_food);
		u_food.setLayoutParams(new LayoutParams(
				(int)(300*rx),(int)(40*ry)));
		u_food.setTextSize((16f*ry)/global.f);		
		LinearLayout ufood_layout = (LinearLayout)findViewById(R.id.about_food_layout);
		LayoutParams ufood_params = new LayoutParams(
				(int)(400*rx),LayoutParams.FILL_PARENT);
		ufood_params.setMargins(0, 0, 0, (int)(5*ry));
		ufood_layout.setLayoutParams(ufood_params);
		ufood_layout.setPadding((int)(100*rx),(int)(0*ry),(int)(0*rx),(int)(0*ry));
		food_yes = (CheckBox)findViewById(R.id.food_yes_checked);
		food_yes.setOnCheckedChangeListener(food_yes_listener);
		food_yes.setTextSize((16f*ry)/global.f);
		food_yes.setLayoutParams(new LinearLayout.LayoutParams(
				(int)(120*rx),(int)(32*ry)));
		food_no = (CheckBox)findViewById(R.id.food_no_checked);
		food_no.setOnCheckedChangeListener(food_no_listener);
		food_no.setTextSize((16f*ry)/global.f);
		food_no.setLayoutParams(new LinearLayout.LayoutParams(
				(int)(120*rx),(int)(32*ry)));
		TextView u_rest = (TextView)findViewById(R.id.about_restaurant_text);
		u_rest.setLayoutParams(new LayoutParams(
				(int)(300*rx),(int)(40*ry)));
		u_rest.setTextSize((16f*ry)/global.f);		
		LinearLayout urest_layout = (LinearLayout)findViewById(R.id.about_restaurant_layout);
		LayoutParams urest_params = new LayoutParams(
				(int)(400*rx),LayoutParams.FILL_PARENT);
		urest_params.setMargins(0, 0, 0, (int)(5*ry));
		urest_layout.setLayoutParams(urest_params);
		urest_layout.setPadding((int)(100*rx),(int)(0*ry),(int)(0*rx),(int)(0*ry));
		restaurant_yes = (CheckBox)findViewById(R.id.restaurant_yes_checked);
		restaurant_yes.setOnCheckedChangeListener(restaurant_yes_listener);
		restaurant_yes.setLayoutParams(new LinearLayout.LayoutParams(
				(int)(120*rx),(int)(32*ry)));
		restaurant_yes.setTextSize((16f*ry)/global.f);
		restaurant_no = (CheckBox)findViewById(R.id.restaurant_no_checked);
		restaurant_no.setOnCheckedChangeListener(restaurant_no_listener);
		restaurant_no.setLayoutParams(new LinearLayout.LayoutParams(
				(int)(120*rx),(int)(32*ry)));
//		restaurant_no.set
		restaurant_no.setTextSize((16f*ry)/global.f);
		
		TextView comment_textView = (TextView)findViewById(R.id.comment_textview);
		LinearLayout.LayoutParams comment_textview_params = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.FILL_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
		comment_textview_params.setMargins((int)(20*rx),(int)(5*ry),0,(int)(5*ry));
		comment_textView.setLayoutParams(comment_textview_params);
		comment_textView.setTextSize((14f*ry)/global.f);
		LinearLayout comment_layout = (LinearLayout)findViewById(R.id.comment_text_layout);
		LinearLayout.LayoutParams comment_layout_params = new LinearLayout.LayoutParams(
				(int)(700*rx),
				(int)(50*ry));
		comment_layout_params.setMargins((int)(20*rx),0,0,0);
		comment_layout.setLayoutParams(comment_layout_params);
		comment_layout.setPadding((int)(2*rx),(int)(2*ry),(int)(2*rx),(int)(2*ry));
		ucomment = (EditText)findViewById(R.id.comment_text);
		ucomment.setTextSize((12f*ry)/global.f);
		LinearLayout button_layout = (LinearLayout)findViewById(R.id.button_layout);
		LinearLayout.LayoutParams button_layout_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		button_layout_params.setMargins(0,(int)(5*ry),(int)(70*rx),0);
		button_layout.setLayoutParams(button_layout_params);
	
		usend = (Button)findViewById(R.id.form_send_button);
		usend.setOnClickListener(usend_listener);
		LinearLayout.LayoutParams usend_params = new LinearLayout.LayoutParams(
				(int)(195*rx),(int)(50*ry));
		usend_params.setMargins(0, 0, (int)(10*rx), 0);
		usend.setLayoutParams(usend_params);
		usend.setTextSize((20f*ry)/global.f);
		uskip = (Button)findViewById(R.id.form_skip_button);
		uskip.setOnClickListener(uskip_listener);
		LinearLayout.LayoutParams uskip_params = new LinearLayout.LayoutParams(
				(int)(195*rx),(int)(50*ry));
//		usend_params.setMargins(0, 0, (int)(10*rx), 0);
		uskip.setLayoutParams(uskip_params);
		uskip.setTextSize((20f*ry)/global.f);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.on_post_form);
		global = new Global();
		rx = global.getRx();
		ry = global.getRy();
		setElements();
		qaTable = (TableLayout)findViewById(R.id.qatable);
		
	}
	public OnCheckedChangeListener restaurant_yes_listener = new OnCheckedChangeListener() {

		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if(isChecked){
				restaurant_no.setChecked(false);
				about_restaurant = "YES";
			}

		}
	};
	public OnCheckedChangeListener restaurant_no_listener = new OnCheckedChangeListener() {

		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if(isChecked){
				restaurant_yes.setChecked(false);
				about_restaurant = "NO";
			}

		}
	};
	public OnCheckedChangeListener food_yes_listener = new OnCheckedChangeListener() {

		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if(isChecked){
				food_no.setChecked(false);
				about_food = "YES";
			}

		}
	};
	public OnCheckedChangeListener food_no_listener = new OnCheckedChangeListener() {

		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if(isChecked){
				food_yes.setChecked(false);
				about_food = "NO";
			}

		}
	};
	public OnCheckedChangeListener menu_yes_listener = new OnCheckedChangeListener() {

		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if(isChecked){
				menu_no.setChecked(false);
				about_menu = "YES";
			}

		}
	};
	public OnCheckedChangeListener menu_no_listener = new OnCheckedChangeListener() {

		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if(isChecked){
				menu_yes.setChecked(false);
				about_menu = "NO";
			}

		}
	};
	public OnCheckedChangeListener yes_checked_listener = new OnCheckedChangeListener() {

		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if(isChecked){
				no_checked.setChecked(false);
				user_isSmoker = "YES";				
			}
		}
	};
	public OnCheckedChangeListener no_checked_listener = new OnCheckedChangeListener() {

		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if(isChecked){
				yes_checked.setChecked(false);
				user_isSmoker = "NO";				
			}
		}
	};
	public OnClickListener occupation_listener = new OnClickListener() {

		public void onClick(View v) {
			startActivity(new Intent("OccupationSpinnerDialog"));
		}
	};

	public OnClickListener uage_listener = new OnClickListener() {

		public void onClick(View v) {
			startActivity(new Intent("AgeSpinnerDialog"));
			//			finish();
		}
	};
	public OnClickListener uskip_listener = new OnClickListener() {

		public void onClick(View v) {
			startActivity(new Intent("ListCategoryView").addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			finish();
		}
	};

	public OnClickListener usend_listener = new OnClickListener() {

		public void onClick(View v) {
			String user_details_str;
			user_name = (uname.getText()).toString();
			user_email = (uemail.getText()).toString();
			user_phone = (uphone.getText()).toString();
			user_occupation = global.getOccupation_str();
			user_age = global.getAge_str();
			user_holiday = (uholiday.getText()).toString();
			user_comment = (ucomment.getText()).toString();
			user_details_str = user_name+
					"-_-"+user_email+
					"-_-"+user_phone+
					"-_-"+user_occupation+
					"-_-"+user_age+
					"-_-"+user_isSmoker+
					"-_-"+user_holiday+
					"-_-"+about_menu+
					"-_-"+about_food+
					"-_-"+about_restaurant+
					"-_-"+user_comment;

			try{
				RestClient client = new RestClient(global.getForm_tableURL());
				client.AddParam("long_str", user_details_str);
				client.Execute(RequestMethod.POST);	

				global.setAge_str("");
				startActivity(new Intent("ListCategoryView").addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				finish();
			}catch(Exception x){

			}


			//			Log.d("TAG",user_name+"\n"+user_occupation+"\n"+user_holiday);
		}
	};
	//	public OnClickListener adview_listener = new OnClickListener(){
	//		public void onClick(View v) {
	//			String link = OP.ads.get(((nextImg-1)%OP.ads.size())).link;
	//			if(!link.startsWith("http"))
	//				link = "http://"+link;
	//			try{
	//				Intent intent = new Intent();
	//				intent.setAction(Intent.ACTION_VIEW);
	//				intent.addCategory(Intent.CATEGORY_BROWSABLE);
	//				intent.setData(Uri.parse(link));
	//				startActivity(intent);
	//
	//			}catch(Exception x){
	//			}
	//		}
	//	};
	//	public OnClickListener stad_listener = new OnClickListener(){
	//		public void onClick(View v) {
	//			try{
	//				Intent intent = new Intent();
	//				intent.setAction(Intent.ACTION_VIEW);
	//				intent.addCategory(Intent.CATEGORY_BROWSABLE);
	//				intent.setData(Uri.parse(OP.stad.link));
	//				startActivity(intent);
	//
	//			}catch(Exception x){
	//			}
	//		}
	//	};

	public OnTouchListener qa_sccroll_listener = new OnTouchListener() {

		public boolean onTouch(View v, MotionEvent event) {
			try{
				if(qa_scroll != null){
					scrollX = qa_scroll.getScrollX();
					scrollY = qa_scroll.getScrollY();
				}
				if(scrollY == 0){
					sc_up.setImageResource(R.drawable.arrow_background_1);
					sc_down.setImageResource(R.drawable.arrow_down_1);

				}
				//250 is width of one element, there r 3 elements 10 extra space
				else if((scrollY+ 44*5+22) > qaTable.getHeight()){
					sc_up.setImageResource(R.drawable.arrow_up_1);
					sc_down.setImageResource(R.drawable.arrow_background_1);
				}else{
					sc_up.setImageResource(R.drawable.arrow_up_1);
					sc_down.setImageResource(R.drawable.arrow_down_1);
				}

			}catch(Exception e){

			}
			return false;
		}
	};
	@Override
	protected void onResume() {
		super.onResume();
		mmTimer = new Timer();
		mmTimer.schedule(new TimerTask() {
			@Override
			public void run() {

				qa_scroll.post(new Runnable() {
					public void run() {
						try{
							if(qa_scroll != null){
								scrollX = qa_scroll.getScrollX();
								scrollY = qa_scroll.getScrollY();
							}
							if(scrollY == 0){
								sc_up.setImageResource(R.drawable.arrow_background_1);
								sc_down.setImageResource(R.drawable.arrow_down_1);

							}
							//250 is width of one element, there r 3 elements 10 extra space
							else if((scrollY+ 44*5+22) > qaTable.getHeight()){
								sc_up.setImageResource(R.drawable.arrow_up_1);
								sc_down.setImageResource(R.drawable.arrow_background_1);
							}

						}catch(Exception e){

						}

					}
				});
			}
		},0,100);
	}

	public void onClick(View arg0) {}
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if(hasFocus){
			if(!(global.getAge_str()).equals(""))
				uage.setText(global.getAge_str());
			if(!(global.getOccupation_str()).equals(""))
				uoccupation.setText(global.getOccupation_str());
		}
	}
	@Override  
	public void onBackPressed() {
		Toast.makeText(PostForm.this, "Press SKIP Button", 3).show();
	}
}
