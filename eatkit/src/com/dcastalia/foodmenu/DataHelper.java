package com.dcastalia.foodmenu;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class DataHelper {

	private static final String DATABASE_NAME = "dcastali_andu.db";
	private static final int DATABASE_VERSION = 1;
	private static final String TABLE_NAME = "food_table";
	private final String TABLE_ROW_ID = "TABLE_ROW_ID";
	static final String id_food = "id_food";
	static final String food_name = "food_name";
	static final String id_cat = "id_cat";
	static final String amount = "amount";
	static final String description = "description";
	static final String food_price = "food_price";
	static final String item_price = "item_price";
	static final String total_price = "total_price";
	
	//http://www.sqlite.org/autoinc.html

	private Context context;
	private SQLiteDatabase db;
	private OpenHelper openHelper = null;

	private SQLiteStatement insertStmt;
	private static final String INSERT = "insert into "
			+ TABLE_NAME + "(id_food, food_name, id_cat, amount, description,item_price, food_price, total_price) values (?,?,?,?,?,?,?,?)";

	public DataHelper(Context context) {
		this.context = context;
		openHelper = new OpenHelper(this.context);
		this.db = openHelper.getWritableDatabase();
		this.insertStmt = this.db.compileStatement(INSERT);
	}

	public long insert(String id_food, String food_name, String id_cat, String amount, String description,String item_price,String food_price, String total_price) {
		this.insertStmt.bindString(1, id_food);
		this.insertStmt.bindString(2, food_name);
		this.insertStmt.bindString(3, id_cat);
		this.insertStmt.bindString(4, amount);
		this.insertStmt.bindString(5, description);
		this.insertStmt.bindString(6, item_price);
		this.insertStmt.bindString(7, food_price);
		this.insertStmt.bindString(8, total_price);
		return this.insertStmt.executeInsert();
	}
	
	/**********************************************************************
	 * UPDATING A ROW IN THE DATABASE TABLE
	 * 
	 * This is an example of how to update a row in the database table
	 * using this class.  You should edit this method to suit your needs.
	 * 
	 * @param rowID the SQLite database identifier for the row to update.
	 * @param rowStringOne the new value for the row's first column
	 * @param rowStringTwo the new value for the row's second column 
	 */ 
	public void updateRow(long rowID, String rowOne, String rowStringTwo, String rowStringThree, String rowStringFour, String rowStringFive, String rowStringSix, String rowStringSeven, String rowStringEight)
	{
		// this is a key value pair holder used by android's SQLite functions
		ContentValues values = new ContentValues();
		values.put(id_food, rowOne);
		values.put(food_name, rowStringTwo);
		values.put(id_cat, rowStringThree);
		values.put(amount, rowStringFour);
		values.put(description, rowStringFive);
		values.put(item_price, rowStringSix);
		values.put(food_price, rowStringSeven);
		values.put(total_price, rowStringEight);
 
		// ask the database object to update the database row of given rowID
		try {db.update(TABLE_NAME, values, TABLE_ROW_ID + "=" + rowID, null);}
		catch (Exception e)
		{
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}
	}
	
	/**********************************************************************
	 * DELETING A ROW FROM THE DATABASE TABLE
	 * 
	 * This is an example of how to delete a row from a database table
	 * using this class. In most cases, this method probably does
	 * not need to be rewritten.
	 * 
	 * @param rowID the SQLite database identifier for the row to delete.
	 */
	public long deleteRow(long rowID)
	{
		// ask the database manager to delete the row of given id
		try {db.delete(TABLE_NAME, TABLE_ROW_ID + "=" + rowID, null);}
		catch (Exception e)
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return rowID;
	}
	
	public void removeAll(){
		db.delete(TABLE_NAME, null, null);
	}
	
	public void deleteAll() {
		this.db.delete(TABLE_NAME, null, null);
	}
	Cursor cursor;
	public List<String> selectAll() {
		List<String> list = new ArrayList<String>();
		cursor = this.db.query(TABLE_NAME, new String[]{"TABLE_ROW_ID","id_food", "food_name", "id_cat", "amount", "description", "food_price","total_price"}, 
				null, null, null, null, null);
		if (cursor.moveToFirst()) {
			do {
				list.add(cursor.getString(0)+"-_-"+cursor.getString(1)+"-_-"+cursor.getString(2)+"-_-"+cursor.getString(3)+
						"-_-"+cursor.getString(4)+"-_-"+cursor.getString(5)+"-_-"+cursor.getString(6)+"-_-"+cursor.getString(7)); 
			} while (cursor.moveToNext());
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return list;
	}
	
	public void close(){
	    if(openHelper!=null)
	        openHelper.close();
	}
	
	private static class OpenHelper extends SQLiteOpenHelper {

		OpenHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("CREATE TABLE " + TABLE_NAME + "(TABLE_ROW_ID INTEGER PRIMARY KEY AUTOINCREMENT, id_food TEXT NOT NULL, food_name TEXT NOT NULL, id_cat TEXT NOT NULL, amount TEXT NOT NULL, description TEXT, item_price TEXT NOT NULL,food_price TEXT NOT NULL, total_price TEXT NOT NULL,created_at TIMESTAMP)");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w("Example", "Upgrading database, this will drop tables and recreate.");
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
			onCreate(db);
		}
	}
}