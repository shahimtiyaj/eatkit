package com.dcastalia.foodmenu;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class MenuParser extends DefaultHandler{

	ArrayList<Menu>mlist = new ArrayList<Menu>();
	int menuId, itemId;
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		if(localName.equals("category")){
			mlist.add(new Menu(
					attributes.getValue("id"), attributes.getValue("name"),
					attributes.getValue("image"),attributes.getValue("desc"),
					attributes.getValue("itemcount")));
			menuId = Integer.parseInt(attributes.getValue("id"));
		}
		if(localName.equals("item")){
			mlist.get(menuId).setItem(
					attributes.getValue("id"), attributes.getValue("name"),
					attributes.getValue("image"),attributes.getValue("desc"),
					attributes.getValue("price"),attributes.getValue("unit"));
			itemId = Integer.parseInt(attributes.getValue("id"));
		}
		if(localName.equals("subitem")){
			mlist.get(menuId).item.get(itemId).setSubItem(
					attributes.getValue("id"), attributes.getValue("name"),
					attributes.getValue("price"),attributes.getValue("unit"));
		}
	}

}
