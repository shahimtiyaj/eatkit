package com.dcastalia.foodmenu;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils.TruncateAt;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.image.SmartImageView;

public class ListCategoryView extends Activity implements OnClickListener , OnTouchListener{

	AnimationDrawable adAnimation;
	Global global;
	/*/*/ListCategoryParser Listparser;
	DataHelper dh;
	private AlertDialog alertDialog;
	int i;
	TextView categoryText;
	private TextView MenuText;
	private TextView ItemText;
	private OptionsParser OP;
	private String unit_price;
	private int nextImg = 0;
	SmartImageView adview;
	SmartImageView logoView;
	Button checkout;
	Button exit;
	Builder EXIT_DIALOG;
	String categoryUrl;
	SmartImageView staticImg;
	TextView open_time, close_time, r_name, r_desc;
	private Timer mTimer;
	private long delayInMili = 5000;
	TableLayout tl;
	TableRow tr;
	TableLayout ftl;
	TableRow ftr;
	boolean elementadded;
	Random rnd;;
	float rx,ry;
	ImageView sc_left,sc_right;
	HorizontalScrollView featured_scroller;
	public void setElements(){
		TableRow cat_text_row = (TableRow)findViewById(R.id.categoryTextview_layout);
		TableRow.LayoutParams cat_text_row_params = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
		cat_text_row_params.setMargins((int)(20*rx),(int)(5*ry),(int)(45*rx),0);
		cat_text_row.setLayoutParams(cat_text_row_params);
		categoryText = (TextView)this.findViewById(R.id.categoryNameText);
		categoryText.setTextSize((18f*ry)/global.f);
		MenuText = (TextView)this.findViewById(R.id.MenuNameText);
		MenuText.setTextSize((18f*ry)/global.f);
		ItemText = (TextView)this.findViewById(R.id.ItemNameText);
		ItemText.setTextSize((18f*ry)/global.f);
		ScrollView ButtonScrollView = (ScrollView)findViewById(R.id.ButtonScrollView);
		LayoutParams ButtonScroller_params = new LayoutParams(LayoutParams.FILL_PARENT,
				(int)(216*ry));
		ButtonScroller_params.setMargins((int)(10*rx), 0, 0, 0);
		ButtonScrollView.setLayoutParams(ButtonScroller_params);

		LinearLayout featured_layout = (LinearLayout)findViewById(R.id.Featured_layout);
		LinearLayout.LayoutParams featured_layout_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, (int)(100*ry));
		featured_layout.setLayoutParams(featured_layout_params);
		featured_scroller = (HorizontalScrollView)findViewById(R.id.FeaturedScrollView);
		featured_scroller.setOnTouchListener(this);
		featured_scroller.setLayoutParams(new LayoutParams(
				(int)(760*rx),LayoutParams.FILL_PARENT));
		sc_left = (ImageView)findViewById(R.id.feature_scroll_left);
		sc_right = (ImageView)findViewById(R.id.feature_scroll_right);
		LayoutParams sc_left_params = new LayoutParams((int)(10*rx),LayoutParams.FILL_PARENT);
		LayoutParams sc_right_params = new LayoutParams((int)(10*rx),LayoutParams.FILL_PARENT);
		sc_left_params.setMargins((int)(1.5*rx),0,0,0);
		sc_right_params.setMargins((int)(4*rx),0,0,0);
		sc_left.setScaleType(ScaleType.FIT_XY);
		sc_right.setScaleType(ScaleType.FIT_XY);
		sc_left.setLayoutParams(sc_left_params);
		sc_right.setLayoutParams(sc_right_params);
		logoView = (SmartImageView)findViewById(R.id.logoview);
		logoView.setLayoutParams(new LayoutParams((int)(64*rx),(int)(64*ry)));
		logoView.setScaleType(ScaleType.FIT_XY);
		checkout = (Button)findViewById(R.id.Checkout);
		checkout.setOnClickListener(checkout_listener);
		LayoutParams checkout_params = new LayoutParams((int)(90*rx),(int)(64*ry));
		checkout_params.setMargins(0, 0, (int)(5*rx), 0);
		checkout.setLayoutParams(checkout_params);
		exit = (Button)findViewById(R.id.Exit);
		exit.setOnClickListener(EXIT_APPLICATION);
		LayoutParams exit_params = new LayoutParams((int)(90*rx),(int)(64*ry));
		exit_params.setMargins(0, 0, (int)(5*rx), 0);
		exit.setLayoutParams(exit_params);
		exit.setTextSize((14f*ry)/global.f);
		adview = (SmartImageView)findViewById(R.id.adImageView);
		adview.setOnClickListener(adview_listener);
		adview.setLayoutParams(new LayoutParams((int)(540*rx),(int)(64*ry)));
		adview.setScaleType(ScaleType.FIT_XY);
		LinearLayout bottom_layout = (LinearLayout)findViewById(R.id.bottom_layout);
		LinearLayout.LayoutParams bottom_layout_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, (int)(64*ry));
		bottom_layout.setLayoutParams(bottom_layout_params);
		bottom_layout.setOrientation(LinearLayout.HORIZONTAL);
		TextView ftTextView = (TextView)findViewById(R.id.FeaturedNavigation);
		LayoutParams ftTextView_params = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		ftTextView_params.setMargins((int)(20*rx),0,0,0);
		ftTextView.setLayoutParams(ftTextView_params);
		ftTextView.setPadding((int)(5*rx), 0, (int)(5*rx), 0);
		ftTextView.setTextSize(20f*ry);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);        
		setContentView(R.layout.listcategoryview);

		global = new Global();
		rx = global.getRx();
		ry = global.getRy();
		setElements();
		EXIT_DIALOG = new AlertDialog.Builder(this);
		tl = (TableLayout)findViewById(R.id.ListCategoryTable);
		ftl = (TableLayout)findViewById(R.id.featuredTable);

		rnd = new Random();

		scrollX = 0;
		scrollY = 0;

		new Loading().execute();

	}

	public LinearLayout ListCategoryButton(String id, String name, String img2, String desc, String item_count){

		LinearLayout parent = new LinearLayout(this);     
		parent.setBackgroundResource(R.drawable.button_background);
		parent.setOrientation(LinearLayout.HORIZONTAL);
		parent.setPadding((int)(15*rx), (int)(12*ry), (int)(6*rx), (int)(2*ry));

		parent.setClickable(true);
		parent.setOnClickListener(category_listener);
		parent.setId(Integer.parseInt(id));
		parent.setLayoutParams(new LayoutParams(
				(int)(250*rx),
				(int)(100*ry)));


		LinearLayout child1 = new LinearLayout(this);
		SmartImageView imgView = new SmartImageView(this);
		imgView.setLayoutParams(new LayoutParams((int)(75*rx),(int)(75*ry)));
		imgView.setImageUrl(img2);
		imgView.setScaleType(ScaleType.FIT_XY);
		child1.setLayoutParams(new LayoutParams(
				LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		child1.addView(imgView);
		parent.addView(child1);

		LinearLayout child2 = new LinearLayout(this);
		child2.setOrientation(LinearLayout.VERTICAL);
		child2.setPadding((int)(2*rx), 0, 0, 0);
		child2.setLayoutParams(new LayoutParams(
				LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));

		TextView text1 = new TextView(this);
		text1.setLayoutParams(new LayoutParams((int)(145*rx),(int)(75*ry)));
		text1.setTextSize((30f*ry)/global.f);
		text1.setText(name);      
		text1.setTextColor(android.graphics.Color.DKGRAY);
		text1.setTypeface(null, android.graphics.Typeface.BOLD);
		text1.setGravity(Gravity.CENTER);
		child2.addView(text1);
		parent.addView(child2);
		return parent;
	}
	public LinearLayout FeatureCategoryButton(String id, String name, String img,
			String desc, String unit_price){

		LinearLayout parent = new LinearLayout(this);     
		parent.setBackgroundResource(R.drawable.button_background);
		parent.setOrientation(LinearLayout.HORIZONTAL);
		//parent.setPadding((int)((15*rx)/global.f), (int)((12*ry)/global.f), (int)((6*rx)/global.f), (int)((2*ry)/global.f));
		parent.setPadding((int)(15*rx), (int)(12*ry), (int)(6*rx), (int)(2*ry));
		parent.setClickable(true);
		parent.setOnClickListener(feature_listener);
		parent.setId(Integer.parseInt(id));
		parent.setLayoutParams(new LayoutParams(
				(int)(250*rx),
				(int)(100*ry)));

		LinearLayout child1 = new LinearLayout(this);
		SmartImageView imgView = new SmartImageView(this);
		imgView.setLayoutParams(new LayoutParams((int)(75*rx),(int)(75*ry)));
		imgView.setImageUrl(img);
		imgView.setScaleType(ScaleType.FIT_XY);
		child1.setLayoutParams(new LayoutParams(
				LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
		child1.addView(imgView);                           child1.setPadding(0, 0,(int)(6*rx) , 0);        
		parent.addView(child1);

		LinearLayout child2 = new LinearLayout(this);
		child2.setOrientation(LinearLayout.VERTICAL);
		child2.setPadding((int)(2*rx), 0, (int)(4*rx), 0);     
		child2.setLayoutParams(new LayoutParams(
				LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));

		TextView text1 = new TextView(this);
		text1.setLayoutParams(new LayoutParams((int)(145*rx),(int)(25*ry)));
		//		text1.setMaxEms(15);
		text1.setTextSize((15f*ry)/global.f);
		text1.setSingleLine();
		text1.setEllipsize(TruncateAt.END);
		text1.setTextColor(android.graphics.Color.BLACK);
		text1.setTypeface(null, android.graphics.Typeface.BOLD);

		text1.setText(name);
		TextView text2 = new TextView(this);
		text2.setLayoutParams(new LayoutParams((int)(145*rx),(int)(25*ry)));
		text2.setTypeface(null, android.graphics.Typeface.BOLD);
		text2.setText(desc);                                   text2.setPadding(0, 0, (int)(4*rx), 0);
		text2.setTextColor(android.graphics.Color.DKGRAY);
		text2.setTextSize((10f*ry)/global.f);
		child2.addView(text1);
		child2.addView(text2);

		LinearLayout child3 = new LinearLayout(this);
		LinearLayout.LayoutParams lp = new LayoutParams(
				(int)(80*rx),
				(int)(20*ry));
		child3.setLayoutParams(lp);
		lp.setMargins(0,(int)(4*ry), 0, 0);
		child3.setBackgroundResource(R.drawable.add_2_bckt);

		LinearLayout child4 = new LinearLayout(this);

		child4.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));
        
		TextView text3 = new TextView(this);
		text3.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));
		text3.setText(unit_price);
		text3.setTextColor(android.graphics.Color.WHITE);
		text3.setTypeface(null,android.graphics.Typeface.BOLD);
		text3.setTextSize((12f*ry)/global.f);
		child3.setPadding((int)(10*rx), 0, (int)(0*rx), 0);
		child3.addView(text3);
		child4.addView(child3);
		child4.setPadding(0, 0, (int)(0*rx), 0);
		child4.setGravity(Gravity.RIGHT|Gravity.BOTTOM);
		child2.addView(child4);
		parent.addView(child2);
		/* Add Button to row. */
		return parent;
		//tr.addView(parent);
	}


	public void CategoryViewGen(){
		try{
			if(Listparser.list.size()>0){
				i=0;
				elementadded = false;
				for(i=0;i<Listparser.list.size();i++){
					//					if((Listparser.list.size()-1)%3 == 0 && (Listparser.list.size()-1) == i){
					//						
					//					}
					if(i%3==0){
						tr = new TableRow(this);
						tr.setLayoutParams(new LayoutParams(
								LayoutParams.FILL_PARENT,
								LayoutParams.WRAP_CONTENT));
						elementadded = false;
						/*if((Listparser.list.size()-1) == i){
							LinearLayout void_layout = new LinearLayout(this);
							void_layout.setLayoutParams(new LayoutParams(
									LayoutParams.FILL_PARENT,
									LayoutParams.WRAP_CONTENT));
							ImageView empty_image = new ImageView(this);
							empty_image.setLayoutParams(new LayoutParams(
									LayoutParams.FILL_PARENT,
									LayoutParams.WRAP_CONTENT));
							empty_image.setImageResource(R.drawable.empty_thumb);
							tr.addView(void_layout);
						}*/
					}
					tr.addView(ListCategoryButton(Listparser.list.get(i).id,Listparser.list.get(i).name,
							Listparser.list.get(i).image,
							Listparser.list.get(i).fileName,"---"));
					if(i%3==2){
						elementadded = true;
						tl.addView(tr,new TableLayout.LayoutParams(
								LayoutParams.FILL_PARENT,
								LayoutParams.WRAP_CONTENT));
					}
				}
				if(!elementadded)
					tl.addView(tr,new TableLayout.LayoutParams(
							LayoutParams.FILL_PARENT,
							LayoutParams.WRAP_CONTENT));
			}else{
				alertDialog = new AlertDialog.Builder(this).create();
				alertDialog.setTitle("Error");  
				alertDialog.setMessage("Unable to fetch main category items!");  
				alertDialog.setButton("OK", new DialogInterface.OnClickListener() {  
					public void onClick(DialogInterface dialog, int which) {  
						finish();
						return;  
					} });   
				alertDialog.show();
			}
		}catch(Exception e){

		}
	}
	public void FeaturedViewGen(){
		try{
			ftr = new TableRow(this);
			ftr.setLayoutParams(new LayoutParams(
					LayoutParams.FILL_PARENT,
					LayoutParams.WRAP_CONTENT));
			if(OP.flist.size()>0){
				for(i=0;i<OP.flist.size();i++){

					unit_price = OP.flist.get(i).unit + " " +OP.flist.get(i).price;
					ftr.addView(FeatureCategoryButton(OP.flist.get(i).id,OP.flist.get(i).name,
							OP.flist.get(i).image, OP.flist.get(i).desc,unit_price));

				}
				if(OP.flist.size()>0)
					ftl.addView(ftr,new TableLayout.LayoutParams(
							LayoutParams.FILL_PARENT,
							LayoutParams.FILL_PARENT));
			}else{
				LinearLayout fll = new LinearLayout(ListCategoryView.this);
				fll.setLayoutParams(new LayoutParams(800,80));
				fll.setBackgroundColor(android.graphics.Color.WHITE);
				TextView fmtv = new TextView(ListCategoryView.this);
				fmtv.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
						LayoutParams.FILL_PARENT));
				fmtv.setText("Sorry! No Featured Item is Listed");
				fmtv.setTextColor(android.graphics.Color.DKGRAY);
				fmtv.setTypeface(null,android.graphics.Typeface.BOLD);
				fmtv.setTextSize((25f*ry)/global.f);
				fmtv.setGravity(Gravity.CENTER);

				fll.addView(fmtv);
				ftr.addView(fll);
				ftl.addView(ftr,new TableLayout.LayoutParams(
						LayoutParams.FILL_PARENT,
						LayoutParams.FILL_PARENT));
			}
		}catch(Exception e){

		}
	}
	public void Listreader(String url){


		try{
			URL website = new URL(url);

			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();
			Listparser = new ListCategoryParser();
			xr.setContentHandler(Listparser);
			//xr.parse(new InputSource(getResources().openRawResource(id)));
			xr.parse(new InputSource(website.openStream()));
			createFile(url);
		}catch (Exception e) {
			Log.d("TAG", "error! "+e);
		}
	}
	
	
	public void ListreaderOff(File file){
		try{

			//URL website = new URL(url);

			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();
			Listparser = new ListCategoryParser();
			//global.setOptionsparser(OP);
			xr.setContentHandler(Listparser);
			//xr.parse(new InputSource(website.openStream()));
			//xr.parse(new InputSource(getResources().openRawResource(id)));
			InputStream in = new BufferedInputStream(new FileInputStream(file));
			xr.parse(new InputSource(in));

		}catch (Exception e) {
			Log.d("TAG", "error! "+e);
		}
	}
	
	
	public void createFile(String url) throws IOException{
		URL url1 = new URL(url);

		//create the new connection  

		HttpURLConnection urlConnection = (HttpURLConnection) url1.openConnection();

		//set up some things on the connection

		urlConnection.setRequestMethod("GET");

		urlConnection.setDoOutput(true);

		//and connect!

		urlConnection.connect();

		//set the path where we want to save the file

		//in this case, going to save it on the root directory of the

		//sd card.
		File sdCard = Environment.getExternalStorageDirectory();
		File dir = new File (sdCard.getAbsolutePath());
		dir.mkdirs();
		

		//File SDCardRoot = new File("/sdcard/"+"Some Folder Name/");

		//create a new file, specifying the path, and the filename

		//which we want to save the file as.

		File file = new File(dir,"meal_types.xml");
		Log.w("myApp", "no network");

		//this will be used to write the downloaded data into the file we created

		FileOutputStream fileOutput = new FileOutputStream(file);

		//this will be used in reading the data from the internet

		InputStream inputStream = urlConnection.getInputStream();

		//this is the total size of the file

		int totalSize = urlConnection.getContentLength();

		//variable to store total downloaded bytes

		int downloadedSize = 0;

		//create a buffer...

		byte[] buffer = new byte[1024];

		int bufferLength = 0; //used to store a temporary size of the buffer

		//now, read through the input buffer and write the contents to the file

		while ( (bufferLength = inputStream.read(buffer)) > 0 ) 

		{

		//add the data in the buffer to the file in the file output stream (the file on the sd card

		fileOutput.write(buffer, 0, bufferLength);

		//add up the size so we know how much is downloaded

		downloadedSize += bufferLength;

		int progress=(int)(downloadedSize*100/totalSize);

		//this is where you would do something to report the prgress, like this maybe

		//updateProgress(downloadedSize, totalSize);

		}

		//close the output stream when done

		fileOutput.close();
	}
	
	


	@Override
	protected void onPause() {
		super.onPause();
		if(mTimer!=null){
			mTimer.cancel();
			mTimer.purge();
		}
	}
	@Override
	protected void onResume() {
		super.onResume();
		mTimer = new Timer();
		mTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				adview.post(new Runnable() {
					public void run() {
						try{
							adview.setImageUrl(OP.ads.get((nextImg = nextImg%OP.ads.size())).image);
							nextImg+=1;	
						}catch(Exception e){

						}

					}
				});
			}
		},0,delayInMili);
		mmTimer = new Timer();
		mmTimer.schedule(new TimerTask() {
			@Override
			public void run() {

				featured_scroller.post(new Runnable() {
					public void run() {
						try{
							if(featured_scroller!=null){
								scrollX = featured_scroller.getScrollX();
								scrollY = featured_scroller.getScrollY();
							}
							if(ftr!=null && ftr.getChildCount()>3){
								if(scrollX == 0){
									sc_right.setImageResource(R.drawable.arrow_right_1);
									sc_left.setImageResource(R.drawable.arrow_background_1);
								}
								//250 is width of one element, there r 3 elements 10 extra space
								else if((scrollX+ (int)(250*rx*3) + 10*rx) >= ftr.getWidth()){
									sc_right.setImageResource(R.drawable.arrow_background_1);
									sc_left.setImageResource(R.drawable.arrow_left_1);
								}
							}
						}catch(Exception e){

						}

					}
				});
			}
		},0,100);
	}

	int scrollX, scrollY;
	Timer mmTimer;
	public OnClickListener adview_listener = new OnClickListener(){
		public void onClick(View v) {
			String link = OP.ads.get(((nextImg-1)%OP.ads.size())).link;
			if(!link.startsWith("http"))
				link = "http://"+link;
			try{
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				intent.addCategory(Intent.CATEGORY_BROWSABLE);
				intent.setData(Uri.parse(link));
				startActivity(intent);

			}catch(Exception x){
			}
		}
	};
	public OnClickListener stad_listener = new OnClickListener(){
		public void onClick(View v) {
			try{
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				intent.addCategory(Intent.CATEGORY_BROWSABLE);
				intent.setData(Uri.parse(OP.stad.link));
				startActivity(intent);

			}catch(Exception x){
			}
		}
	};
	public OnClickListener category_listener = new OnClickListener(){

		public void onClick(View v) {
			global.setListId(v.getId());
			global.setFeaturedItemClicked(false);
			startActivity(new Intent("MenuView").addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
			if(dh!=null){
				dh.close();
			}
		}
	};

	public OnClickListener feature_listener = new OnClickListener() {

		public void onClick(View v) {
			global.setFeaturedItemClicked(true);
			global.setFeatureId(v.getId());
			startActivity(new Intent("PopupView"));
		}
	};

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if(hasFocus){
			try{
				if((global.getOptionsparser()).equals(null))
					System.exit(0);
			}catch(Exception e){
				System.exit(0);
			}

		}
	}

	public OnClickListener checkout_listener = new OnClickListener() {

		public void onClick(View v) {
			if(global.getCount()>0||global.getTobaccoList().size()>0)
				startActivity(new Intent("ConfWin"));
			else
				Toast.makeText(ListCategoryView.this, "Please select an item", 5).show();      
		}
	};


	public OnClickListener EXIT_APPLICATION = new OnClickListener() {

		/*public void onClick(View v) {
			EXIT_DIALOG
			.setTitle("EXIT APPLICATION")  
			.setMessage("Do you really want to exit?")  
			.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					try{
						System.runFinalizersOnExit(true);
						System.exit(0);
						//android.os.Process.killProcess(android.os.Process.myPid());
					}catch(Exception e){

					}
				}
			})
			.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					//do stuff onclick of YES
					return;
					//finish();
				}
			}).show();

		}*/
		public void onClick(View v) {
			Intent i = new Intent(ListCategoryView.this,FoodMenu.class);
			startActivity(i);
			finish();
		}
	};
	@Override  
	public void onBackPressed() {
		EXIT_DIALOG
		.setTitle("EXIT APPLICATION")  
		.setMessage("Do you really want to exit?")  
		.setPositiveButton("YES", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				try{
					//					ListCategoryView.this.onBackPressed();
					//					finish();
					System.runFinalizersOnExit(true);
					System.exit(0);
					//android.os.Process.killProcess(android.os.Process.myPid());
				}catch(Exception e){

				}
			}
		})
		.setNegativeButton("NO", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				//do stuff onclick of YES
				return;
				//finish();
			}
		}).show();

	}
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		//		Log.v("ON_TOUCH", "Touched");
		return super.onTouchEvent(event);
	}
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		return super.dispatchTouchEvent(ev);
	}
	boolean toggle = true;
	public boolean onTouch(View v, MotionEvent event) {

		if(ftr!=null && ftr.getChildCount()>3){

			if(scrollX == 0){
				sc_right.setImageResource(R.drawable.arrow_right_1);
				sc_left.setImageResource(R.drawable.arrow_background_1);
			}
			else if((scrollX+ (int)(250*rx*3) + 10*rx) >= ftr.getWidth()){
				sc_right.setImageResource(R.drawable.arrow_background_1);
				sc_left.setImageResource(R.drawable.arrow_left_1);
			}else{
				sc_right.setImageResource(R.drawable.arrow_right_1);
				sc_left.setImageResource(R.drawable.arrow_left_1);
			}

		}

		return false;
	}
	public void onClick(View v) {

	}

	class Loading extends AsyncTask<String, Void, Void>{
		ProgressDialog dialog = new ProgressDialog(ListCategoryView.this);
		@Override
		protected Void doInBackground(String... params) {
			if(global.isOnline){
				try{
					categoryUrl = global.getMeal_typeXML()
							+"?p="+rnd.nextInt();
					Listreader(categoryUrl);
					if(!(global.getOptionsparser()).equals(null))
						global.setListparser(Listparser);
					OP = global.getOptionsparser();
				}catch(Exception e){
					Log.e("ERROR",e.toString());
				}
			}
			else{
				//ListreaderOff(R.raw.meal_type);
				File sdCard = Environment.getExternalStorageDirectory();
				File dir = new File (sdCard.getAbsolutePath());
				File file = new File(dir,"meal_types.xml");
				ListreaderOff(file);

				
			if(!(global.getOptionsparser()).equals(null))
				global.setListparser(Listparser);
			OP = global.getOptionsparser();}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try{
				adview.setImageUrl(OP.ads.get((nextImg = nextImg%OP.ads.size())).image);
				logoView.setImageUrl(OP.restaurent.logo);

				categoryText.setText("EatKit Category >> ");
				MenuText.setText("");
				ItemText.setText("");
				logoView.setImageUrl(OP.restaurent.logo);
				FeaturedViewGen();
				CategoryViewGen();

			}catch(Exception e){

			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try{
				if(!(global.getOptionsparser()).equals(null))
				{
					this.dialog.setMessage("Fetching data...");
					this.dialog.show();	
				}	
			}catch(Exception e){

			}


		}

	}
}
