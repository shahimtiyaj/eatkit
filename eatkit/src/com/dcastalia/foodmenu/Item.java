package com.dcastalia.foodmenu;

import java.util.ArrayList;

public class Item {

	String id;
	String name;
	String image;
	String desc;
	String price;
	String unit;

	ArrayList<Subitem>subitem = new ArrayList<Subitem>();
	
	public Item(String id, String name, String image, String desc, String price, String unit){
		this.id = id;
		this.name = name;
		this.image = image;
		this.desc = desc;
		this.price = price;
		this.unit = unit;
	}

	public void setSubItem(String id, String name, String price, String unit){
		subitem.add(new Subitem(id, name, price, unit));
	}
	public String getId(){
		return id;
	}

	public String getName(){
		return name;
	}

	public String getImage(){
		return image;
	}

	public String getDesc(){
		return desc;
	}

	public String getPrice(){
		return price;
	}

	public String getUnit(){
		return unit;
	}
}

