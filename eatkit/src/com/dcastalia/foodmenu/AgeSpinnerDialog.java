package com.dcastalia.foodmenu;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;

public class AgeSpinnerDialog extends Activity{

	Global global;
	float rx,ry;
	Button close_button;
	String values[]={"15-20","21-27","28-35","36-45","above 46"};
	public void setElements(){
		LinearLayout main_spin_layout = (LinearLayout)findViewById(R.id.main_spin_layout);
		main_spin_layout.setLayoutParams(new LayoutParams((int)(280*rx),(int)(350*ry)));
		LinearLayout top_spin_layout = (LinearLayout)findViewById(R.id.top_layout);
		top_spin_layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,(int)(60*ry)));
		ImageView ic_menu_imageview = (ImageView)findViewById(R.id.ic_menu_imageview);
		ic_menu_imageview.setLayoutParams(new LayoutParams((int)(50*rx),(int)(50*ry)));
		TextView age_select_textView = (TextView)findViewById(R.id.age_textview);
		LayoutParams age_select_params = new LayoutParams((int)(180*rx),LayoutParams.FILL_PARENT);
//		amount_select_params.setMargins((int)(50*rx),0,0,0);
		age_select_textView.setLayoutParams(age_select_params);
		age_select_textView.setTextSize((20f*ry)/global.f);
		close_button = (Button)findViewById(R.id.close);
		LayoutParams close_Params = new LayoutParams(
				(int)(32*rx),(int)(32*ry));
		close_Params.setMargins((int)(10*rx),0,0,0);
		close_button.setLayoutParams(close_Params);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.age_spinner_dialog_view);
		global = new Global();
		rx = global.getRx();
		ry = global.getRy();
		setElements();
//		close_button = (Button)findViewById(R.id.close);
		
		ListView lv = (ListView)findViewById(R.id.agespinnerList);
		 lv.setAdapter(new SpinnerDialogArrayAdapter(AgeSpinnerDialog.this, values));
	        lv.setOnItemClickListener(new OnItemClickListener() {

	            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
	                    long arg3) {
	            	global.setAge_str(values[position]);
	            	finish();
	            }

	        });
	        close_button.setOnClickListener(close_button_listener);
	}
	public OnClickListener close_button_listener = new OnClickListener() {

		public void onClick(View arg0) {
			finish();
		}
	};
	@Override  
	public void onBackPressed() {
		//Do Nothing
	}
}
