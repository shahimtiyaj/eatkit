package com.dcastalia.foodmenu;

import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.image.SmartImageView;

public class ItemView extends Activity implements OnClickListener,OnTouchListener{
	AnimationDrawable adAnimation;
	String [] values;
	private ListView Lister;
	int menuid, itemid;
	Global global;
	Button close;
	Button checkout;
	Button Exit;
	Button Back;
	TextView ItemText;
	MenuParser Menuparser; 
	TableLayout bckt_table;
	String unit_price;
	ListCategoryParser Listparser;
	OptionsParser OP;
	int i;
	private AlertDialog alertDialog;
	int scrollX, scrollY;
	Timer mmTimer;
	private TextView categoryText;
	private TextView MenuText;

	TextView ItemName;
	TextView ItemDesc;
	TextView catstr;
	ImageView ItemImage;
	String checkvalue[];
	SmartImageView staticImg;
	SmartImageView adview;
	SmartImageView logoView;
	TextView open_time, close_time, r_name, r_desc;

	ImageView sc_down,sc_up;
	private Timer mTimer;
	private long delayInMili = 5000;

	private int nextImg = 0;
	float rx,ry;
	Builder EXIT_DIALOG;
	ScrollView item_scroller;

	public void setElements(){

		LinearLayout main_layout = (LinearLayout)findViewById(R.id.main_layout);
		LinearLayout.LayoutParams main_layout_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, (int)(390*ry));
		main_layout.setLayoutParams(main_layout_params);
		main_layout.setOrientation(LinearLayout.HORIZONTAL);

		LinearLayout main_layout_left = (LinearLayout)findViewById(R.id.main_layout_left);
		LinearLayout.LayoutParams main_layout_left_params = new LinearLayout.LayoutParams
				((int)(600*rx),LinearLayout.LayoutParams.FILL_PARENT);
		main_layout_left_params.setMargins(0, (int)(5*ry), 0, 0);
		main_layout_left.setLayoutParams(main_layout_left_params);
		main_layout_left.setOrientation(LinearLayout.VERTICAL);

		LinearLayout main_layout_right = (LinearLayout)findViewById(R.id.main_layout_right);
		LinearLayout.LayoutParams main_layout_right_params = new LinearLayout.LayoutParams
				((int)(185*rx),LinearLayout.LayoutParams.FILL_PARENT);
		main_layout_right_params.setMargins((int)(5*rx), (int)(5*ry), 0, (int)(5*ry));
		main_layout_right.setLayoutParams(main_layout_right_params);
		main_layout_right.setOrientation(LinearLayout.VERTICAL);

		LinearLayout bottom_layout = (LinearLayout)findViewById(R.id.bottom_layout);
		LinearLayout.LayoutParams bottom_layout_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, (int)(64*ry));
		bottom_layout.setLayoutParams(bottom_layout_params);
		bottom_layout.setOrientation(LinearLayout.HORIZONTAL);
		logoView = (SmartImageView)findViewById(R.id.logoview);
		logoView.setLayoutParams(new LayoutParams((int)(64*rx),(int)(64*ry)));
		logoView.setScaleType(ScaleType.FIT_XY);
		adview = (SmartImageView)findViewById(R.id.adImageView);
		adview.setOnClickListener(adview_listener);
		adview.setLayoutParams(new LayoutParams((int)(540*rx),(int)(64*ry)));
		adview.setScaleType(ScaleType.FIT_XY);

		item_scroller = (ScrollView)findViewById(R.id.ItemScrollView);
		item_scroller.setOnTouchListener(this);
		LayoutParams item_scroller_params = new LayoutParams(LayoutParams.FILL_PARENT,
				(int)(340*ry));
		item_scroller_params.setMargins((int)(10*rx), 0, 0, 0);
		item_scroller.setLayoutParams(item_scroller_params);
		item_scroller.setVerticalFadingEdgeEnabled(false);

		checkout = (Button)this.findViewById(R.id.Checkout);
		checkout.setOnClickListener(checkout_listener);

		LayoutParams checkout_params = new LayoutParams((int)(90*rx),(int)(64*ry));
		checkout_params.setMargins(0, 0, (int)(5*rx), 0);
		checkout.setLayoutParams(checkout_params);

		Exit = (Button)this.findViewById(R.id.Exit);
		Exit.setOnClickListener(EXIT_APPLICATION);

		LayoutParams exit_params = new LayoutParams((int)(90*rx),(int)(64*ry));
		exit_params.setMargins(0, 0, (int)(5*rx), 0);
		Exit.setLayoutParams(exit_params);
		Exit.setTextSize((16f*(ry-.1f))/global.f);
		LinearLayout list_layout = (LinearLayout)findViewById(R.id.list_layout);
		LinearLayout.LayoutParams list_layout_params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, (int)(320*ry));
		list_layout_params.setMargins(0, 0, 0, (int)(2*ry));
		list_layout.setLayoutParams(list_layout_params);

		Back = (Button)findViewById(R.id.BackButton);
		Back.setOnClickListener(Back_Button_Listener);
		LayoutParams back_params = new LayoutParams(LayoutParams.FILL_PARENT,
				(int)((45*ry)/global.f));
		back_params.setMargins((int)(1.5*rx), 0, (int)(1.5*rx), 0);
		Back.setLayoutParams(back_params);

		categoryText = (TextView)this.findViewById(R.id.categoryNameText);
		categoryText.setOnClickListener(categoryText_click_listener);
		categoryText.setTextSize((18f*ry)/global.f);
		MenuText = (TextView)this.findViewById(R.id.MenuNameText);
		MenuText.setOnClickListener(MenuText_click_listener);
		MenuText.setTextSize((18f*ry)/global.f);
		ItemText = (TextView)this.findViewById(R.id.ItemNameText);
		ItemText.setTextSize((18f*ry)/global.f);
		catstr = (TextView)findViewById(R.id.catstr);
		catstr.setOnClickListener(categoryText_click_listener);
		catstr.setTextSize((18f*ry)/global.f);
		sc_down = (ImageView)findViewById(R.id.item_scroll_down);
		sc_up = (ImageView)findViewById(R.id.item_scroll_up);
		LayoutParams sc_down_params = new LayoutParams(LayoutParams.FILL_PARENT,(int)(10*ry));
		LayoutParams sc_up_params = new LayoutParams(LayoutParams.FILL_PARENT,(int)(10*ry));
		sc_down_params.setMargins((int)(15*rx),0,0,0);
		sc_up_params.setMargins((int)(15*rx),0,0,0);
		sc_down.setScaleType(ScaleType.FIT_XY);
		sc_up.setScaleType(ScaleType.FIT_XY);
		sc_down.setLayoutParams(sc_down_params);
		sc_up.setLayoutParams(sc_up_params);
		TableRow cat_text_row = (TableRow)findViewById(R.id.categoryTextview_layout);
		TableRow.LayoutParams cat_text_row_params = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
		cat_text_row_params.setMargins((int)(20*rx),0,0,0);
		cat_text_row.setLayoutParams(cat_text_row_params);

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);        
		this.setContentView(R.layout.item);
		global = new Global();
		rx = global.getRx();
		ry = global.getRy();
		s = global.getBckt();
		menuid = global.getMenuId();
		OP = global.getOptionsparser();
		EXIT_DIALOG = new AlertDialog.Builder(this);
		Listparser = global.getListparser();
		Menuparser = global.getMenuparser();

		if((Menuparser.mlist.get(global.getMenuId()).name).equals("BENSON & HEDGES")){
			global.getMenu_ctx().finish();
		}
		setElements();
		try{
			categoryText.setText("EatKit Category >> ");
			MenuText.setText(Listparser.list.get(global.getListId()).getName()+" Items >> ");
			if((Menuparser.mlist.get(global.getMenuId()).name).equals("BENSON & HEDGES"))
				ItemText.setText("");
			else
				ItemText.setText(Menuparser.mlist.get(menuid).getName());
			logoView.setImageUrl(OP.restaurent.logo);
			adview.setImageUrl(OP.ads.get((nextImg = nextImg%OP.ads.size())).image);
		}catch(Exception e){}

		values = new String[Listparser.list.size()];

		for(int i=0;i<values.length;i++){
			values[i] = Listparser.list.get(i).name;
		}

		Lister = (ListView)findViewById(R.id.List);
		MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(this, values);
		Lister.setAdapter(adapter);
		Lister.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				if(global.getListId()!=position){
					global.setListId(position);
					global.getMenu_ctx().finish();
					startActivity(new Intent("MenuView"));
					finish();
				}else if(!(Menuparser.mlist.get(global.getMenuId()).name).equals("BENSON & HEDGES")){
					finish();
				}
			}

		});

		itl = (TableLayout)findViewById(R.id.ItemTable);
		ftl = (TableLayout)findViewById(R.id.featuredTable);

		if(Menuparser.mlist.get(menuid).item.size()>0){

			i=0;
			elementadded = false;
			String unit_price;
			for(i=0;i<Menuparser.mlist.get(menuid).item.size();i++){
				if(i%2==0){
					itr = new TableRow(this);
					itr.setLayoutParams(new LayoutParams(
							LayoutParams.FILL_PARENT,
							LayoutParams.WRAP_CONTENT));
					elementadded = false;
				}
				unit_price = Menuparser.mlist.get(menuid).item.get(i).unit+"  "+
						Menuparser.mlist.get(menuid).item.get(i).price;
				itr.addView(ItemButton(Menuparser.mlist.get(menuid).item.get(i).id,
						Menuparser.mlist.get(menuid).item.get(i).name,
						Menuparser.mlist.get(menuid).item.get(i).image,
						Menuparser.mlist.get(menuid).item.get(i).desc,
						unit_price));
				if(i%2==1){
					elementadded = true;
					itl.addView(itr,new TableLayout.LayoutParams(
							LayoutParams.FILL_PARENT,
							LayoutParams.WRAP_CONTENT));
				}
			}
			if(!elementadded)
				itl.addView(itr,new TableLayout.LayoutParams(
						LayoutParams.FILL_PARENT,
						LayoutParams.WRAP_CONTENT));
		}else{
			alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("Error");  
			alertDialog.setMessage("Please check your network connection");  
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {  
				public void onClick(DialogInterface dialog, int which) {  
					finish();
					return;  
				} });   
			alertDialog.show();
		}
		item_scroller.setOnTouchListener(this);
	}
	TableLayout itl;
	TableRow itr;

	TableLayout ftl;
	TableRow ftr;

	boolean elementadded;

	public LinearLayout ItemButton(String id, String name, String img2, String desc, String unit_price){

		LinearLayout parent = new LinearLayout(this);     
		parent.setBackgroundResource(R.drawable.button_background);
		parent.setOrientation(LinearLayout.HORIZONTAL);
		parent.setPadding((int)(18*rx), (int)(12*ry), (int)(6*rx), (int)(2*ry));
		parent.setClickable(true);
		parent.setOnClickListener(item_listener);
		parent.setId(Integer.parseInt(id));
		parent.setLayoutParams(new LayoutParams(
				(int)(300*rx),
				(int)(100*ry)));


		LinearLayout child1 = new LinearLayout(this);
		SmartImageView imgView = new SmartImageView(this);
		imgView.setImageUrl(img2);
		if((Menuparser.mlist.get(global.getMenuId()).name).equals("BENSON & HEDGES"))
			imgView.setLayoutParams(new LayoutParams((int)(75*rx),(int)(55*ry)));
		else
			imgView.setLayoutParams(new LayoutParams((int)(75*rx),(int)(75*ry)));
		imgView.setScaleType(ScaleType.FIT_XY);
		child1.setLayoutParams(new LayoutParams(
				LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
		child1.addView(imgView);
		child1.setPadding((int)(0*rx), (int)(0*ry), (int)(10*rx), (int)(0*ry));

		parent.addView(child1);

		LinearLayout child2 = new LinearLayout(this);
		child2.setOrientation(LinearLayout.VERTICAL);
		//		child2.setPadding(0, 3, 0, 3);
		child2.setLayoutParams(new LayoutParams(
				LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));

		TextView text1 = new TextView(this);
		text1.setLayoutParams(new LayoutParams((int)(190*rx),(int)(25*ry)));
		text1.setTextSize((15f*ry)/global.f);
		text1.setTextColor(android.graphics.Color.BLACK);
		text1.setTypeface(null, android.graphics.Typeface.BOLD);
		text1.setText(name);
		TextView text2 = new TextView(this);
		text2.setLayoutParams(new LayoutParams((int)(190*rx),(int)(25*ry)));
		text2.setText(desc);
		text2.setTextColor(android.graphics.Color.DKGRAY);
		text2.setTypeface(null, android.graphics.Typeface.BOLD);
		text2.setTextSize((10f*ry)/global.f);
		child2.addView(text1);
		child2.addView(text2);


		LinearLayout child3 = new LinearLayout(this);
		child3.setBackgroundResource(R.drawable.add_2_bckt);
		LinearLayout.LayoutParams lp = new LayoutParams((int)(80*rx), (int)(20*ry));
		child3.setLayoutParams(lp);
		lp.setMargins(0,(int)(4*ry), 0, 0);		

		LinearLayout child4 = new LinearLayout(this);
		child4.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT));

		TextView text3 = new TextView(this);
		text3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
		text3.setText(unit_price);
		text3.setTextColor(android.graphics.Color.WHITE);
		text3.setTypeface(null,android.graphics.Typeface.BOLD);
		text3.setTextSize((12f*ry)/global.f);
		child3.setPadding((int)(10*rx), 0, 0, 0);
		child3.addView(text3);
		child4.addView(child3);
		child4.setPadding(0, 0, (int)(4*rx), 0);
		child4.setGravity(Gravity.RIGHT|Gravity.BOTTOM);
		child2.addView(child4);
		parent.addView(child2);

		return parent;

	}
	DecimalFormat format = new DecimalFormat("###.##");
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if(hasFocus){

		}

	}

	public OnClickListener checkout_listener = new OnClickListener() {

		public void onClick(View v) {
			try{
				if(global.getCount()>0||global.getTobaccoList().size()>0)
					startActivity(new Intent("ConfWin"));
				else
					Toast.makeText(ItemView.this, "Please select an item", 5).show();    
			}catch(Exception x){
				Log.d("TAG",x.toString());
			}

		}
	};
	public OnClickListener EXIT_APPLICATION = new OnClickListener() {

		public void onClick(View v) {
			EXIT_DIALOG
			.setTitle("EXIT APPLICATION")  
			.setMessage("Do you really want to exit?")  
			.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					try{
						global.getMenu_ctx().finish();
						System.runFinalizersOnExit(true);
						System.exit(0);
						//android.os.Process.killProcess(android.os.Process.myPid());
					}catch(Exception e){

					}
				}
			})
			.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					//do stuff onclick of YES
					return;
					//finish();
				}
			}).show();

		}
	};
	public OnClickListener categoryText_click_listener = new OnClickListener(){

		public void onClick(View v) {
			global.getMenu_ctx().finish();
			//startActivity(new Intent("ListCategoryView"));
			finish();
		}

	};
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		//		global.getMenu_ctx().finish();
		finish();
	}
	public OnClickListener MenuText_click_listener = new OnClickListener() {

		public void onClick(View v) {
			finish();
		}
	};

	public OnClickListener item_listener = new OnClickListener() {

		public void onClick(View v) {
			global.setFeaturedItemClicked(false);
			global.setItemId(v.getId());
			itemid = global.getItemId();    
			startActivity(new Intent("PopupView"));
		}
	};
	/*
	 * 		s+=global.getBckt();
		global.setBckt(s);
		t.setText("");
		if(!(s.equals("")))
			t.setText(click_text+s);
	 */
	public void onClick(View v) {
		//Log.d("TAG","clicked!");
	}

	String click_text = " ITEMS CHOSEN: ";
	String s ="";
	@Override
	protected void onPause() {
		super.onPause();
		if(mTimer!=null){
			mTimer.cancel();
			mTimer.purge();
		}
	}
	@Override
	protected void onResume() {
		super.onResume();

		mTimer = new Timer();
		mTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				adview.post(new Runnable() {
					public void run() {
						adview.setImageUrl(OP.ads.get((nextImg = nextImg%OP.ads.size())).image);
						nextImg+=1;
					}
				});
			}
		},0,delayInMili);
		mmTimer = new Timer();
		mmTimer.schedule(new TimerTask() {
			@Override
			public void run() {

				item_scroller.post(new Runnable() {
					public void run() {
						try{
							if(item_scroller != null){
								scrollX = item_scroller.getScrollX();
								scrollY = item_scroller.getScrollY();
							}
							if(itl!=null&& itl.getChildCount()>3){
								if(scrollY == 0){
									sc_up.setImageResource(R.drawable.arrow_background_1);
									sc_down.setImageResource(R.drawable.arrow_down_1);

								}
								//250 is width of one element, there r 3 elements 10 extra space
								else if((scrollY+ (int)(100*ry*3) + 50*ry) > itl.getHeight()){
									sc_up.setImageResource(R.drawable.arrow_up_1);
									sc_down.setImageResource(R.drawable.arrow_background_1);
								}
							}
						}catch(Exception e){

						}

					}
				});
			}
		},0,100);
	}

	public OnClickListener stad_listener = new OnClickListener(){
		public void onClick(View v) {
			try{
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				intent.addCategory(Intent.CATEGORY_BROWSABLE);
				intent.setData(Uri.parse(OP.stad.link));
				startActivity(intent);

			}catch(Exception x){
			}
		}
	};
	public OnClickListener Back_Button_Listener = new OnClickListener() {

		public void onClick(View arg0) {
			finish();
		}
	};
	public OnClickListener adview_listener = new OnClickListener(){
		public void onClick(View v) {
			String link = OP.ads.get(((nextImg-1)%OP.ads.size())).link;
			if(!link.startsWith("http"))
				link = "http://"+link;
			try{
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				intent.addCategory(Intent.CATEGORY_BROWSABLE);
				intent.setData(Uri.parse(link));
				startActivity(intent);
			}catch(Exception x){
			}
		}
	};

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		//		Log.v("ON_TOUCH", "Touched");
		return super.onTouchEvent(event);
	}
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		//		Log.v("ON_TOUCH", "Action = " + ev.getAction());
		return super.dispatchTouchEvent(ev);
	}
	boolean toggle = true;
	public boolean onTouch(View v, MotionEvent event) {
		//		int action = event.getAction();
		float x = event.getX();
		float y = event.getY();
		if(itl!=null&& itl.getChildCount()>3){
			if(scrollY == 0 ){
				sc_up.setImageResource(R.drawable.arrow_background_1);
				sc_down.setImageResource(R.drawable.arrow_down_1);

			}
			//250 is width of one element, there r 3 elements 10 extra space
			else if((scrollY+ (int)(100*ry*3) + 50*ry) >= itl.getHeight()){
				sc_up.setImageResource(R.drawable.arrow_up_1);
				sc_down.setImageResource(R.drawable.arrow_background_1);
			}else{
				sc_up.setImageResource(R.drawable.arrow_up_1);
				sc_down.setImageResource(R.drawable.arrow_down_1);
			}
		}
		//		if(itl!=null&&itl.getChildCount()>3&&itl.getChildCount()<=6){
		//			if((v.getBottom()/2-y)>0){
		//				sc_down.setImageResource(R.drawable.arrow_background);
		//				sc_up.setImageResource(R.drawable.arrow_up_1);
		//			}
		//			else{
		//				sc_down.setImageResource(R.drawable.arrow_down_1);
		//				sc_up.setImageResource(R.drawable.arrow_background);
		//			}
		//		}
		//		if(itl!=null&&itl.getChildCount()>6){
		//			sc_up.setImageResource(R.drawable.arrow_up_1);
		//			sc_down.setImageResource(R.drawable.arrow_down_1);
		//		}
		//	    Log.v("ON_TOUCH", "Action = " + action + " View:" + v.toString());
		//	    Log.v("ON_TOUCH", "X = " + x + "Y = " + y+"Y_AXIS = "+Y_AXIS);
		//	    Log.v("ON_TOUCH", (v.getBottom()/2-y)+"");
		//	    Log.v("ON_TOUCH", itl.getBottom()+" is TABLE BOTTOM");
		//	    Toast.makeText(ItemView.this, v.getId()+"", 1).show();
		return false;
	}
}