package com.dcastalia.foodmenu;

public class Subitem {
	String id;
	String name;
	String price;
	String unit;
	
	public Subitem(String id, String name, String price, String unit){
		this.id = id;
		this.name = name;
		this.price = price;
		this.unit = unit;
	}
	public String getId(){
		return id;
	}

	public String getName(){
		return name;
	}

	public String getPrice(){
		return price;
	}

	public String getUnit(){
		return unit;
	}
	
}
