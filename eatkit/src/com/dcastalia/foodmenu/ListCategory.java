package com.dcastalia.foodmenu;

public class ListCategory {

	String id;
	String name;
	String fileName;
	String image;
	
	public ListCategory(String id, String name, String fileName,String image){
		this.id = id;
		this.name = name;
		this.fileName = fileName;
		this.image = image;
	}
	
	public String getImage(){
		return image;
	}
	
	public String getId(){
		return id;
	}
	
	public String getName(){
		return name;
	}
	
	public String getFileName(){
		return fileName;
	}
}
